package main

import (
	"context"
	"errors"
	"fmt"
	"runtime"
	"time"

	pg "go-processors/common/postgres"

	"github.com/jackc/pgx/v4"
)

var TASK_CHANNELS = make([]chan struct{}, 0)

var ALL_QUEUES_TO_RUN = []QueueTask{
	ClusterATask{},
	ClusterBTask{},
	ClusterPreTask{},
	ClusterCTask{},
}

func main() {
	pg.ConnectToPostgres()

	retrieveFlags()
	fmt.Println("[DEBUG] Flag tables loaded")

	for _, task := range ALL_QUEUES_TO_RUN {
		task_chan := make(chan struct{}, task.NumParallel())
		TASK_CHANNELS = append(TASK_CHANNELS, task_chan)

		go func(qt QueueTask, qt_chan chan struct{}) {
			fmt.Printf("[DEBUG] Starting up queue task '%s'\n", qt.Name())
			qt.LoadProfile()
			fmt.Printf("[DEBUG] Started up task '%s'\n", qt.Name())

			for {
				qt_chan <- struct{}{}

				qtInfo, tx := prepareTransaction(qt)
				fmt.Printf("[TRACE] Task retrieved for '%s': %#v\n", qt.Name(), qtInfo)

				itemsToAdd := qt.GetItems(qtInfo)
				for _, item := range itemsToAdd {
					err := qt.AddToQueue(tx, item)
					if err != nil {
						panic(err)
					}
					fmt.Printf("[TRACE] Task '%s' added to queue: %#v\n", qt.Name(), item)
				}

				if err := tx.Commit(context.Background()); err != nil {
					panic(err)
				}

				time.Sleep(time.Duration(qt.WaitMillisecondsEnds()) * time.Millisecond)
				<-qt_chan
			}
		}(task, task_chan)
	}

	runtime.Goexit()

	fmt.Println("[DEBUG] Exiting program...")
}

func prepareTransaction(qt QueueTask) (QueueTInfo, pgx.Tx) {
	tx, err := pg.Client.BeginTx(context.Background(), pgx.TxOptions{})
	if err != nil {
		panic(err)
	}

	fmt.Printf("[INFO] Preparing queue task '%s'\n", qt.Name())
	qtInfo, err := qt.Prepare(tx)
	for err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			fmt.Printf("[WARN] Task '%s' gone idle, waiting %d seconds instead...\n", qt.Name(), qt.WaitSecondsIdle())

			err = tx.Commit(context.Background())
			if err != nil {
				panic(err)
			}

			time.Sleep(time.Duration(qt.WaitSecondsIdle()) * time.Second)
		} else {
			fmt.Printf("Error from %s\n", qt.Name())
			panic(err)
		}

		tx, err = pg.Client.BeginTx(context.Background(), pgx.TxOptions{})
		if err != nil {
			panic(err)
		}

		qtInfo, err = qt.Prepare(tx)
	}

	return *qtInfo, tx
}
