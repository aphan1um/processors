package main

import (
	"context"
	pg "go-processors/common/postgres"
)

var postgresClusterAFlagMap = make(map[string]int)
var postgresClusterBFlagMap = make(map[string]int)
var postgresClusterCFlagMap = make(map[string]int)

func retrieveFlags() {
	// cluster A flags
	rows, _ := pg.Client.Query(context.Background(), "SELECT flag_id,flag_name FROM \"ClusterAFlag\"")
	for rows.Next() {
		var (
			flag_id   int
			flag_name string
		)
		rows.Scan(&flag_id, &flag_name)
		postgresClusterAFlagMap[flag_name] = flag_id
	}
	rows.Close()

	// cluster B flags
	rows, _ = pg.Client.Query(context.Background(), "SELECT flag_id,flag_name FROM \"ClusterBFlag\"")
	for rows.Next() {
		var (
			flag_id   int
			flag_name string
		)
		rows.Scan(&flag_id, &flag_name)
		postgresClusterBFlagMap[flag_name] = flag_id
	}
	rows.Close()

	// cluster C or company flags
	rows, _ = pg.Client.Query(context.Background(), "SELECT flag_id,flag_name FROM \"CompanyFlag\"")
	for rows.Next() {
		var (
			flag_id   int
			flag_name string
		)
		rows.Scan(&flag_id, &flag_name)
		postgresClusterCFlagMap[flag_name] = flag_id
	}
	rows.Close()
}
