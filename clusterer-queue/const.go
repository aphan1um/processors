package main

import (
	"go-processors/common"
	"strconv"
)

var MAX_JOB_GROUPS_QUEUE, _ = strconv.Atoi(common.GetEnv("MAX_JOB_GROUPS_QUEUE", "5"))

var CLUSTER_A_PROFILE = common.GetEnv("CLUSTER_A_PROFILE", "")
var CLUSTER_B_PROFILE = common.GetEnv("CLUSTER_B_PROFILE", "")
