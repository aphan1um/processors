package main

import (
	"context"
	"errors"
	"fmt"
	pg "go-processors/common/postgres"

	"github.com/jackc/pgx/v4"
)

var CLUSTER_B_QUERY string

const CLUSTER_B_MAX_ENTRIES = 5
const CLUSTER_B_REPOLL_MINS = 15

type ClusterBTask struct{}

func (t ClusterBTask) Name() string {
	return "ClusterBTask"
}

func (t ClusterBTask) NumParallel() int {
	return 2
}

func (t ClusterBTask) WaitSecondsIdle() int {
	return 45
}

func (t ClusterBTask) WaitMillisecondsEnds() int {
	return 400
}

func (t ClusterBTask) LoadProfile() {
	if len(CLUSTER_B_PROFILE) == 0 {
		panic("CLUSTER_B_PROFILE is empty or not provided")
	}

	// https://shiroyasha.io/selecting-for-share-and-update-in-postgresql.html
	CLUSTER_B_QUERY = fmt.Sprintf(
		`SELECT cluster_b_id, flag
		FROM "ClusterB"
		WHERE
			config_id = '%s' AND
			NOW() - updated_at > INTERVAL '%d minute' AND
			flag < (SELECT POWER(2, MAX(flag_id) + 1) - 1 FROM "ClusterBFlag") AND
			cluster_b_id NOT IN (SELECT cluster_b_id FROM "ClusterBQueue")
		LIMIT 1
		FOR SHARE OF "ClusterB" SKIP LOCKED`,
		CLUSTER_B_PROFILE,
		CLUSTER_B_REPOLL_MINS)

	fmt.Println(CLUSTER_B_QUERY)
}

func (t ClusterBTask) Prepare(tx pgx.Tx) (*QueueTInfo, error) {
	// check there are not too many rows
	var num_entries int
	err := pg.Client.QueryRow(
		context.Background(),
		"SELECT COUNT(DISTINCT cluster_b_id) AS num_entries FROM \"ClusterBQueue\" INNER JOIN \"ClusterB\" USING (cluster_b_id) WHERE config_id = $1 LIMIT $2",
		CLUSTER_B_PROFILE, CLUSTER_B_MAX_ENTRIES).Scan(&num_entries)
	if !errors.Is(err, pgx.ErrNoRows) && err != nil {
		panic(err)
	} else if num_entries >= CLUSTER_B_MAX_ENTRIES {
		return nil, pgx.ErrNoRows
	}

	var cluster_b_id int
	var flag int
	err = pg.QuerySQLRowTx(CLUSTER_B_QUERY, false, []interface{}{}, tx).Scan(&cluster_b_id, &flag)

	if err != nil {
		return nil, err
	}

	return &QueueTInfo{
		"cluster_b_id": cluster_b_id,
		"flag":         flag,
	}, nil
}

func (t ClusterBTask) GetItems(tcb QueueTInfo) []QueueTItem {
	items := make([]QueueTItem, 0)
	for _, flag_id := range postgresClusterBFlagMap {
		if (1<<flag_id)&tcb["flag"].(int) == 0 {
			items = append(items, QueueTItem{
				"info":    tcb,
				"flag_id": flag_id,
			})
		}
	}
	return items
}

func (t ClusterBTask) AddToQueue(tx pgx.Tx, tci QueueTItem) error {
	_, err := tx.Exec(
		context.Background(),
		"INSERT INTO \"ClusterBQueue\" (cluster_b_id, flag_id) VALUES ($1, $2) ON CONFLICT DO NOTHING",
		tci["info"].(QueueTInfo)["cluster_b_id"].(int),
		tci["flag_id"].(int))
	return err
}
