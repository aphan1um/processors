package main

import (
	"github.com/jackc/pgx/v4"
)

type QueueTInfo map[string]interface{}
type QueueTItem map[string]interface{}

type QueueTask interface {
	Name() string
	NumParallel() int
	WaitSecondsIdle() int
	WaitMillisecondsEnds() int
	LoadProfile()
	Prepare(pgx.Tx) (*QueueTInfo, error)
	GetItems(QueueTInfo) []QueueTItem
	AddToQueue(pgx.Tx, QueueTItem) error
}
