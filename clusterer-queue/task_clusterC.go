package main

import (
	"context"
	"errors"
	"fmt"
	pg "go-processors/common/postgres"

	"github.com/jackc/pgtype"
	"github.com/jackc/pgx/v4"
)

var CLUSTER_C_QUERY string

const CLUSTER_C_MAX_ENTRIES = 5
const CLUSTER_C_REPOLL_MINS = 30

type ClusterCTask struct{}

func (t ClusterCTask) Name() string {
	return "ClusterCTask"
}

func (t ClusterCTask) NumParallel() int {
	return 2
}

func (t ClusterCTask) WaitSecondsIdle() int {
	return 50
}

func (t ClusterCTask) WaitMillisecondsEnds() int {
	return 600
}

func (t ClusterCTask) LoadProfile() {
	// https://shiroyasha.io/selecting-for-share-and-update-in-postgresql.html
	CLUSTER_C_QUERY = fmt.Sprintf(
		`SELECT company_id, flag, abn_id
		FROM "Company"
		WHERE
			NOW() - updated_at > INTERVAL '%d minute' AND
			flag < (SELECT POWER(2, MAX(flag_id) + 1) - 1 FROM "CompanyFlag") AND
			company_id NOT IN (SELECT company_id FROM "CompanyQueue")
		LIMIT 1
		FOR SHARE OF "Company" SKIP LOCKED`,
		CLUSTER_C_REPOLL_MINS)

	fmt.Println(CLUSTER_C_QUERY)
}

func (t ClusterCTask) Prepare(tx pgx.Tx) (*QueueTInfo, error) {
	// check there are not too many rows
	var num_entries int
	err := pg.Client.QueryRow(
		context.Background(),
		`SELECT COUNT(DISTINCT company_id) FROM "CompanyQueue" LIMIT $1`,
		CLUSTER_C_MAX_ENTRIES).Scan(&num_entries)
	if !errors.Is(err, pgx.ErrNoRows) && err != nil {
		panic(err)
	} else if num_entries >= CLUSTER_C_MAX_ENTRIES {
		return nil, pgx.ErrNoRows
	}

	var company_id int
	var flag int
	var abn_id pgtype.Int8
	err = pg.QuerySQLRowTx(CLUSTER_C_QUERY, false, []interface{}{}, tx).Scan(&company_id, &flag, &abn_id)

	if err != nil {
		return nil, err
	}

	return &QueueTInfo{
		"company_id": company_id,
		"flag":       flag,
		"abn_id":     abn_id,
	}, nil
}

func (t ClusterCTask) GetItems(tcc QueueTInfo) []QueueTItem {
	items := make([]QueueTItem, 0)
	for flag_name, flag_id := range postgresClusterCFlagMap {
		// FLAG_DNB requires an ABN to have been found for the company
		if flag_name == "FLAG_DNB" && tcc["abn_id"].(pgtype.Int8).Status != pgtype.Present {
			continue
		}

		if (1<<flag_id)&tcc["flag"].(int) == 0 {
			items = append(items, QueueTItem{
				"info":    tcc,
				"flag_id": flag_id,
			})
		}
	}
	return items
}

func (t ClusterCTask) AddToQueue(tx pgx.Tx, tci QueueTItem) error {
	_, err := tx.Exec(
		context.Background(),
		"INSERT INTO \"CompanyQueue\" (company_id, flag_id) VALUES ($1, $2) ON CONFLICT DO NOTHING",
		tci["info"].(QueueTInfo)["company_id"].(int),
		tci["flag_id"].(int))
	return err
}
