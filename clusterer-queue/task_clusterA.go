package main

import (
	"context"
	"errors"
	"fmt"
	pg "go-processors/common/postgres"

	"github.com/jackc/pgx/v4"
)

var CLUSTER_A_QUERY string

const CLUSTER_A_MAX_ENTRIES = 5
const CLUSTER_A_REPOLL_MINS = 15

type ClusterATask struct{}

func (t ClusterATask) Name() string {
	return "ClusterATask"
}

func (t ClusterATask) NumParallel() int {
	return 2
}

func (t ClusterATask) WaitSecondsIdle() int {
	return 45
}

func (t ClusterATask) WaitMillisecondsEnds() int {
	return 400
}

func (t ClusterATask) LoadProfile() {
	// https://shiroyasha.io/selecting-for-share-and-update-in-postgresql.html
	CLUSTER_A_QUERY = fmt.Sprintf(
		`SELECT cluster_a_id, flag
		FROM "ClusterA"
		WHERE
			config_id = '%s' AND
			NOW() - updated_at > INTERVAL '%d minute' AND
			flag < (SELECT POWER(2, MAX(flag_id) + 1) - 1 FROM "ClusterAFlag") AND
			cluster_a_id NOT IN (SELECT cluster_a_id FROM "ClusterAQueue")
		LIMIT 1
		FOR SHARE OF "ClusterA" SKIP LOCKED`,
		CLUSTER_A_PROFILE,
		CLUSTER_A_REPOLL_MINS)

	fmt.Println(CLUSTER_A_QUERY)
}

func (t ClusterATask) Prepare(tx pgx.Tx) (*QueueTInfo, error) {
	// check there are not too many rows
	var num_entries int

	err := pg.Client.QueryRow(
		context.Background(),
		`SELECT COUNT(DISTINCT cluster_a_id)
		FROM "ClusterAQueue" INNER JOIN "ClusterA" USING (cluster_a_id)
		WHERE config_id = $1 LIMIT $2`,
		CLUSTER_A_PROFILE, CLUSTER_A_MAX_ENTRIES).Scan(&num_entries)
	if !errors.Is(err, pgx.ErrNoRows) && err != nil {
		panic(err)
	} else if num_entries >= CLUSTER_A_MAX_ENTRIES {
		return nil, pgx.ErrNoRows
	}

	var cluster_a_id int
	var flag int
	err = pg.QuerySQLRowTx(CLUSTER_A_QUERY, false, []interface{}{}, tx).Scan(&cluster_a_id, &flag)

	if err != nil {
		return nil, err
	}

	return &QueueTInfo{
		"cluster_a_id": cluster_a_id,
		"flag":         flag,
	}, nil
}

func (t ClusterATask) GetItems(tca QueueTInfo) []QueueTItem {
	items := make([]QueueTItem, 0)
	for _, flag_id := range postgresClusterAFlagMap {
		if (1<<flag_id)&tca["flag"].(int) == 0 {
			items = append(items, QueueTItem{
				"info":    tca,
				"flag_id": flag_id,
			})
		}
	}
	return items
}

func (t ClusterATask) AddToQueue(tx pgx.Tx, tci QueueTItem) error {
	_, err := tx.Exec(
		context.Background(),
		`INSERT INTO "ClusterAQueue" (cluster_a_id, flag_id) VALUES ($1, $2) ON CONFLICT DO NOTHING`,
		tci["info"].(QueueTInfo)["cluster_a_id"].(int),
		tci["flag_id"].(int))
	return err
}
