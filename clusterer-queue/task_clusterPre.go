package main

import (
	"context"
	"errors"
	"fmt"

	pg "go-processors/common/postgres"

	"github.com/jackc/pgx/v4"
)

var CLUSTER_PRE_QUERY string
var CLUSTER_PRE_MAX_ENTRIES = 10

type ClusterPreTask struct{}

func (t ClusterPreTask) Name() string {
	return "ClusterPreTask"
}

func (t ClusterPreTask) NumParallel() int {
	return 3
}

func (t ClusterPreTask) WaitSecondsIdle() int {
	return 60
}

func (t ClusterPreTask) WaitMillisecondsEnds() int {
	return 600
}

func (t ClusterPreTask) LoadProfile() {
	if len(CLUSTER_A_PROFILE) == 0 {
		panic("CLUSTER_A_PROFILE is empty or not provided")
	}

	CLUSTER_PRE_QUERY = fmt.Sprintf(
		`SELECT
		TRIM(LOWER(data->>'company')) FROM jobs
		WHERE
			job_id NOT IN (SELECT job_id FROM "ClusterJobList" INNER JOIN "ClusterA" USING (cluster_a_id) WHERE config_id = '%s') AND
			TRIM(LOWER(data->>'company')) NOT IN (SELECT company_name FROM "ClusterPreQueue")
		LIMIT 1`,
		CLUSTER_A_PROFILE)

	fmt.Println(CLUSTER_PRE_QUERY)
}

func (t ClusterPreTask) Prepare(tx pgx.Tx) (*QueueTInfo, error) {
	// check there are not too many rows
	var num_entries int
	err := pg.Client.QueryRow(
		context.Background(),
		"SELECT COUNT(*) FROM \"ClusterPreQueue\" WHERE config_id = $1 LIMIT $2",
		CLUSTER_A_PROFILE, CLUSTER_PRE_MAX_ENTRIES).Scan(&num_entries)
	if !errors.Is(err, pgx.ErrNoRows) && err != nil {
		panic(err)
	} else if num_entries >= CLUSTER_PRE_MAX_ENTRIES {
		return nil, pgx.ErrNoRows
	}

	var company_name string
	err = pg.QuerySQLRowTx(CLUSTER_PRE_QUERY, false, []interface{}{}, tx).Scan(&company_name)

	if err != nil {
		return nil, err
	}

	return &QueueTInfo{
		"company_name": company_name,
	}, nil
}

func (t ClusterPreTask) GetItems(qinfo QueueTInfo) []QueueTItem {
	return []QueueTItem{
		{
			"company_name": qinfo["company_name"].(string),
		},
	}
}

func (t ClusterPreTask) AddToQueue(tx pgx.Tx, qitem QueueTItem) error {
	_, err := tx.Exec(
		context.Background(),
		"INSERT INTO \"ClusterPreQueue\" (company_name, config_id) VALUES ($1, $2) ON CONFLICT DO NOTHING",
		qitem["company_name"].(string),
		CLUSTER_A_PROFILE)
	return err
}
