package main

import (
	"context"
	"go-processors/common/api"
	pgUtils "go-processors/common/postgres"

	"github.com/jackc/pgx/v4"
)

type ApiWorkType struct{}

func (a ApiWorkType) FlagName() string {
	return "FLAG_WORK_TYPE"
}

func (a ApiWorkType) Cluster() byte {
	return 'B'
}

func (a ApiWorkType) ApiPath() string {
	return "/job/type"
}

func (a ApiWorkType) Server() api.ApiServer {
	return api.ServerJobs
}

func (a ApiWorkType) GetDocList(id int, flag_id int, tx pgx.Tx) []interface{} {
	return retrieveJobMetaClusterB(id, flag_id, tx)
}

func (a ApiWorkType) GetDoc(meta interface{}, tx pgx.Tx) pgUtils.JobDoc {
	return retrieveJobDoc(meta, tx)
}

func (a ApiWorkType) GetBodyRequest(job_doc pgUtils.JobDoc, collectionName string, group_id int) api.BodyRequest {
	return api.BodyRequest{
		"value":          job_doc.JobType,
		"collectionName": collectionName,
	}
}

func (a ApiWorkType) PreSelectDoc() func(pgUtils.JobDoc) bool {
	return nil
}

func (a ApiWorkType) GotEnoughInfo(m api.BodyResponse) bool {
	return true
}

func (a ApiWorkType) UpdateDatabase(response api.BodyResponse, doc pgUtils.JobDoc, cluster_b_id int, tx pgx.Tx) {
	_, err := tx.Exec(
		context.Background(),
		`INSERT INTO "ClusterB" (cluster_b_id, jobtype_id) VALUES ($1, $2) ON CONFLICT (cluster_b_id) DO UPDATE SET jobtype_id = $2`,
		cluster_b_id,
		postgresJobTypeMap[response["work_type"].(string)])
	if err != nil {
		panic(err)
	}
}
