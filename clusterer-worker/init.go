package main

import (
	"context"
	pg "go-processors/common/postgres"
)

var postgresJobTypeMap = make(map[string]int)
var postgresPhraseTypeMap = make(map[string]int)
var postgresWageTypeMap = make(map[string]int)
var postgresEducationLevelMap = make(map[string]int)

var postgresClusterAFlagMap = make(map[string]int)
var postgresClusterBFlagMap = make(map[string]int)
var postgresClusterCFlagMap = make(map[string]int)

func loadProfile() {
	var profile map[string]interface{}

	if len(CLUSTER_B_PROFILE) == 0 {
		panic("CLUSTER_B_PROFILE is empty or not provided")
	}

	err := pg.Client.QueryRow(
		context.Background(),
		"SELECT meta FROM \"ClusterConfig\" WHERE config_id = $1",
		CLUSTER_B_PROFILE).Scan(&profile)
	if err != nil {
		panic(err)
	}

	SOFTSKILL_CONFIG = profile["softskills"].(map[string]interface{})
}

func retrieveDataTables() {
	_retrieveJobTypeValues()
	_retrieveWageTypeValues()
	_retrievePhraseTypeValues()
	_retrieveEducationLevelValues()
	_retrieveClusterFlags()
}

func _retrieveJobTypeValues() {
	rows, _ := pg.Client.Query(context.Background(), "SELECT jobtype_id,jobtype FROM infojobtype")
	defer rows.Close()
	for rows.Next() {
		var (
			jobtype_id int
			jobtype    string
		)
		rows.Scan(&jobtype_id, &jobtype)
		postgresJobTypeMap[jobtype] = jobtype_id
	}
}

func _retrieveWageTypeValues() {
	rows, _ := pg.Client.Query(context.Background(), "SELECT wagetype_id,title FROM infowagetypes")
	defer rows.Close()
	for rows.Next() {
		var (
			wagetype_id int
			title       string
		)
		rows.Scan(&wagetype_id, &title)
		postgresWageTypeMap[title] = wagetype_id
	}
}

func _retrievePhraseTypeValues() {
	rows, _ := pg.Client.Query(context.Background(), `SELECT phrasetype_id,phrasetype FROM "PhraseType"`)
	defer rows.Close()
	for rows.Next() {
		var (
			phrasetype_id int
			phrasetype    string
		)
		rows.Scan(&phrasetype_id, &phrasetype)
		postgresPhraseTypeMap[phrasetype] = phrasetype_id
	}
}

func _retrieveEducationLevelValues() {
	rows, _ := pg.Client.Query(context.Background(), "SELECT edulevel_id,title FROM infoeducationlevel")
	defer rows.Close()
	for rows.Next() {
		var (
			edulevel_id int
			title       string
		)
		rows.Scan(&edulevel_id, &title)
		postgresEducationLevelMap[title] = edulevel_id
	}
}

func _retrieveClusterFlags() {
	// cluster A flags
	rows, _ := pg.Client.Query(context.Background(), `SELECT flag_id,flag_name FROM "ClusterAFlag"`)
	for rows.Next() {
		var (
			flag_id   int
			flag_name string
		)
		rows.Scan(&flag_id, &flag_name)
		postgresClusterAFlagMap[flag_name] = flag_id
	}
	rows.Close()

	// cluster B flags
	rows, _ = pg.Client.Query(context.Background(), `SELECT flag_id,flag_name FROM "ClusterBFlag"`)
	for rows.Next() {
		var (
			flag_id   int
			flag_name string
		)
		rows.Scan(&flag_id, &flag_name)
		postgresClusterBFlagMap[flag_name] = flag_id
	}
	rows.Close()

	// cluster C or company flags
	rows, _ = pg.Client.Query(context.Background(), `SELECT flag_id,flag_name FROM "CompanyFlag"`)
	for rows.Next() {
		var (
			flag_id   int
			flag_name string
		)
		rows.Scan(&flag_id, &flag_name)
		postgresClusterCFlagMap[flag_name] = flag_id
	}
	rows.Close()
}
