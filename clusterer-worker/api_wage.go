package main

import (
	"context"
	"go-processors/common/api"
	pgUtils "go-processors/common/postgres"

	"github.com/jackc/pgx/v4"
)

type ApiWage struct{}

func (a ApiWage) FlagName() string {
	return "FLAG_SALARY"
}

func (a ApiWage) Cluster() byte {
	return 'B'
}

func (a ApiWage) ApiPath() string {
	return "/job/pay"
}

func (a ApiWage) Server() api.ApiServer {
	return api.ServerJobs
}

func (a ApiWage) GetBodyRequest(job_doc pgUtils.JobDoc, collectionName string, group_id int) api.BodyRequest {
	return api.BodyRequest{
		"jobTitle":    job_doc.Title,
		"description": job_doc.Description,
		"payString":   job_doc.PayType,
	}
}

func (a ApiWage) PreSelectDoc(doc pgUtils.JobDoc) bool {
	return true
}

func (a ApiWage) GotEnoughInfo(m api.BodyResponse) bool {
	return m["rate"] != nil
}

func (a ApiWage) UpdateDatabase(response api.BodyResponse, cluster_b_id int, tx pgx.Tx) {
	if response["rate"] == nil {
		return
	}

	var lowWageSQL interface{} = nil
	var highWageSQL interface{} = nil

	if response["low"] != nil {
		lowWageSQL = response["low"]
		highWageSQL = response["low"]

		if response["high"] != nil {
			highWageSQL = response["high"]
		}
	}

	_, err := tx.Exec(
		context.Background(),
		`INSERT INTO "ClusterBWage" (cluster_b_id, wagetype_id, low_wage, high_wage)
		VALUES ($1, $2, $3, $4)
		ON CONFLICT (cluster_b_id)
		DO UPDATE SET wagetype_id = $2, low_wage = $3, high_wage = $4`,
		cluster_b_id, postgresWageTypeMap[response["rate"].(string)], lowWageSQL, highWageSQL)
	if err != nil {
		panic(err)
	}
}
