package main

import (
	"context"
	"fmt"
	"go-processors/common/api"
	pgUtils "go-processors/common/postgres"
	"log"
	"strconv"
	"strings"

	"github.com/jackc/pgx/v4"
)

type ApiAbn struct{}

func (a ApiAbn) FlagName() string {
	return "FLAG_ABN"
}

func (a ApiAbn) Cluster() byte {
	return 'C'
}

func (a ApiAbn) ApiPath() string {
	return "/google/abn/detailed"
}

func (a ApiAbn) Server() api.ApiServer {
	return api.ServerGeo
}

func (a ApiAbn) GetDocList(id int, flag_id int, tx pgx.Tx) []interface{} {
	return retrieveCompanyMeta(id, flag_id, tx)
}

func (a ApiAbn) GetDoc(meta interface{}, tx pgx.Tx) pgUtils.JobDoc {
	return retrieveCompanyDoc(meta, tx)
}

func (a ApiAbn) GetBodyRequest(job_doc pgUtils.JobDoc, collectionName string, group_id int) api.BodyRequest {
	// TODO: Add location
	return api.BodyRequest{
		"query":    job_doc.Company,
		"location": "",
	}
}

func (a ApiAbn) PreSelectDoc() func(pgUtils.JobDoc) bool {
	return nil
}

func (a ApiAbn) GotEnoughInfo(m api.BodyResponse) bool {
	return true
}

func (a ApiAbn) UpdateDatabase(response api.BodyResponse, doc pgUtils.JobDoc, company_id int, tx pgx.Tx) {
	switch response["status"].(string) {
	case "success":

		abn_id, err := strconv.Atoi(response["abn"].(string))
		if err != nil {
			panic(err)
		}

		// insert abn entry
		// (only if abn entry hasn't been added, in that case it could be there are >= 2 companies
		// with the same abn reference)
		_, err = tx.Exec(
			context.Background(),
			`INSERT INTO "CompanyABN" (abn_id, entity_id, entity_name, poa_code) VALUES ($1, $2, $3, $4)
			ON CONFLICT DO NOTHING`,
			abn_id,
			response["entityType"].(string),
			strings.TrimSpace(strings.ToLower(response["name"].(string))),
			int(response["location"].(map[string]interface{})["postcode"].(float64)),
		)
		if err != nil {
			panic(err)
		}

		// associate abn with company
		_, err = tx.Exec(
			context.Background(),
			`UPDATE "Company" SET abn_id = $1 WHERE company_id = $2`,
			abn_id,
			company_id)
		if err != nil {
			panic(err)
		}

	// TODO: Improve non-successful ANZSIC calls
	case "noresult":
		fmt.Printf("[WARN] Unable to find any ABN with name that could confirm its firm type for company ID %d\n", company_id)
	case "failure":
		log.Fatalf("[ERROR] Unable to find any ABN with company ID %d\n", company_id)
	}
}
