package main

import (
	"context"
	"go-processors/common/api"
	pgUtils "go-processors/common/postgres"

	"github.com/jackc/pgx/v4"
)

type ApiAge struct{}

func (a ApiAge) FlagName() string {
	return "FLAG_AGE"
}

func (a ApiAge) Cluster() byte {
	return 'B'
}

func (a ApiAge) ApiPath() string {
	return "/age"
}

func (a ApiAge) Server() api.ApiServer {
	return api.ServerJobs
}

func (a ApiAge) GetDocList(id int, flag_id int, tx pgx.Tx) []interface{} {
	return retrieveJobMetaClusterB(id, flag_id, tx)
}

func (a ApiAge) GetDoc(meta interface{}, tx pgx.Tx) pgUtils.JobDoc {
	return retrieveJobDoc(meta, tx)
}

func (a ApiAge) GetBodyRequest(job_doc pgUtils.JobDoc, collectionName string, group_id int) api.BodyRequest {
	return api.BodyRequest{
		"title":       job_doc.Title,
		"description": job_doc.Description,
		"meta": map[string]interface{}{
			"groupId": group_id,
			"webId":   job_doc.JobMeta.JobID,
		},
	}
}

func (a ApiAge) PreSelectDoc() func(pgUtils.JobDoc) bool {
	return nil
}

func (a ApiAge) GotEnoughInfo(m api.BodyResponse) bool {
	return true
}

func (a ApiAge) UpdateDatabase(response api.BodyResponse, doc pgUtils.JobDoc, cluster_b_id int, tx pgx.Tx) {
	var age_min *int = nil
	var age_max *int = nil

	if response["age_info"] != nil {
		if val, ok := response["age_info"].(map[string]interface{})["age_min"]; ok {
			age_min = new(int)
			*age_min = int(val.(float64))
		}

		if val, ok := response["age_info"].(map[string]interface{})["age_max"]; ok {
			age_max = new(int)
			*age_max = int(val.(float64))
		}
	}

	_, err := tx.Exec(
		context.Background(),
		`INSERT INTO "ClusterBAge" (cluster_b_id, age_min, age_max) VALUES ($1, $2, $3)`,
		cluster_b_id, age_min, age_max,
	)
	if err != nil {
		panic(err)
	}
}
