package main

import (
	"context"
	"fmt"
	"go-processors/common/api"
	pgUtils "go-processors/common/postgres"

	"github.com/jackc/pgx/v4"
)

type ApiSoftSkills struct{}

func (a ApiSoftSkills) FlagName() string {
	return "FLAG_SOFT_SKILLS"
}

func (a ApiSoftSkills) Cluster() byte {
	return 'A'
}

func (a ApiSoftSkills) ApiPath() string {
	return "/semantics/soft-skills"
}

func (a ApiSoftSkills) Server() api.ApiServer {
	return api.ServerJobs
}

func (a ApiSoftSkills) GetDocList(id int, flag_id int, tx pgx.Tx) []interface{} {
	return retrieveJobMetaClusterA(id, flag_id, tx)
}

func (a ApiSoftSkills) GetDoc(meta interface{}, tx pgx.Tx) pgUtils.JobDoc {
	return retrieveJobDoc(meta, tx)
}

func (a ApiSoftSkills) GetBodyRequest(job_doc pgUtils.JobDoc, collectionName string, group_id int) api.BodyRequest {
	return api.BodyRequest{
		"text": job_doc.Description,
		"conf": SOFTSKILL_CONFIG,
	}
}

func (a ApiSoftSkills) PreSelectDoc() func(pgUtils.JobDoc) bool {
	return nil
}

func (a ApiSoftSkills) GotEnoughInfo(m api.BodyResponse) bool {
	return true
}

func (a ApiSoftSkills) UpdateDatabase(response api.BodyResponse, doc pgUtils.JobDoc, group_id int, tx pgx.Tx) {
	v, ok := response["softskill_scores"].(map[string]interface{})
	if !ok {
		panic("Error casting soft skills dict (i.e. something is really wrong)")
	}

	sqlInsertPhrases(v, group_id, "softskill", tx)
}

func sqlInsertPhrases(phrases api.BodyResponse, cluster_a_id int, phraseType string, tx pgx.Tx) {
	for phrase, value := range phrases {
		var phrase_id int
		phrasetype_id := postgresPhraseTypeMap[phraseType]

		// see if phrase exists
		err := pgUtils.QuerySQLRowTx(
			`SELECT phrase_id FROM "Phrases" WHERE phrase_name = $1 AND phrasetype_id = $2`,
			false, []interface{}{phrase, phrasetype_id}, tx).Scan(&phrase_id)

		if err == pgx.ErrNoRows { // if phrase hasn't been added yet
			err = pgUtils.QuerySQLRowTx(
				`INSERT INTO "Phrases" (phrasetype_id, phrase_name) VALUES ($1, $2) RETURNING phrase_id`,
				false,
				[]interface{}{phrasetype_id, phrase},
				tx).Scan(&phrase_id)
		}
		if err != nil {
			panic(err)
		}

		fmt.Printf(
			"[Phrase] Phrase '%s' (type: %s, type ID: %d) found with phrase ID %d and value %f\n",
			phrase, phraseType, phrasetype_id, phrase_id, value)

		// now we can add phrase
		_, err = tx.Exec(
			context.Background(),
			`INSERT INTO "ClusterAPhraseList" (cluster_a_id, phrase_id, value) VALUES ($1, $2, $3)
			ON CONFLICT (cluster_a_id, phrase_id) DO UPDATE SET value = $3`,
			cluster_a_id, phrase_id, value)
		if err != nil {
			panic(err)
		}
	}
}
