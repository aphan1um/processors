package main

import (
	"go-processors/common"
)

var JOBS_API_URL = common.GetEnv("JOBS_API_URL", "http://localhost:8000")
var GEO_API_URL = common.GetEnv("GEO_API_URL", "http://localhost:3000")

var CLUSTER_B_PROFILE = common.GetEnv("CLUSTER_B_PROFILE", "")

var SOFTSKILL_CONFIG map[string]interface{}
