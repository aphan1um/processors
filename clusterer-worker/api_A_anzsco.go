package main

import (
	"context"
	"go-processors/common/api"
	pgUtils "go-processors/common/postgres"
	"strings"

	"github.com/jackc/pgx/v4"
)

type ApiAnzsco struct{}

func (a ApiAnzsco) FlagName() string {
	return "FLAG_ANZSCO"
}

func (a ApiAnzsco) Cluster() byte {
	return 'A'
}

func (a ApiAnzsco) ApiPath() string {
	return "/job/anzsco"
}

func (a ApiAnzsco) Server() api.ApiServer {
	return api.ServerJobs
}

func (a ApiAnzsco) GetDocList(id int, flag_id int, tx pgx.Tx) []interface{} {
	return retrieveJobMetaClusterA(id, flag_id, tx)
}

func (a ApiAnzsco) GetDoc(meta interface{}, tx pgx.Tx) pgUtils.JobDoc {
	return retrieveJobDoc(meta, tx)
}

func (a ApiAnzsco) GetBodyRequest(job_doc pgUtils.JobDoc, collectionName string, group_id int) api.BodyRequest {
	return api.BodyRequest{
		"title":       job_doc.Title,
		"description": job_doc.Description,
		"category":    job_doc.Category,
		"subcategory": job_doc.Subcategory,
		"company":     job_doc.Company,
	}
}

func (a ApiAnzsco) PreSelectDoc() func(pgUtils.JobDoc) bool {
	return func(doc pgUtils.JobDoc) bool {
		return len(strings.TrimSpace(doc.Category)) > 0
	}
}

func (a ApiAnzsco) GotEnoughInfo(m api.BodyResponse) bool {
	return true
}

func (a ApiAnzsco) UpdateDatabase(response api.BodyResponse, doc pgUtils.JobDoc, cluster_a_id int, tx pgx.Tx) {
	_, err := tx.Exec(
		context.Background(),
		`UPDATE "ClusterA" SET anzsco_id = $2 WHERE cluster_a_id = $1`,
		cluster_a_id, int(response["anzsco_code"].(float64)),
	)
	if err != nil {
		panic(err)
	}
}
