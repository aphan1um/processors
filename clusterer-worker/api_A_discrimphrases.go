package main

import (
	"go-processors/common/api"
	pgUtils "go-processors/common/postgres"
	"strings"

	"github.com/jackc/pgx/v4"
)

type ApiDiscrimPhrases struct{}

var DISCRIMINATING_PHRASE_TYPES = []string{"gender", "disability"}

func (a ApiDiscrimPhrases) FlagName() string {
	return "FLAG_DISCRIMINATORY_PHRASES"
}

func (a ApiDiscrimPhrases) Cluster() byte {
	return 'A'
}

func (a ApiDiscrimPhrases) ApiPath() string {
	return "/discriminatory-counts"
}

func (a ApiDiscrimPhrases) Server() api.ApiServer {
	return api.ServerJobs
}

func (a ApiDiscrimPhrases) GetDocList(id int, flag_id int, tx pgx.Tx) []interface{} {
	return retrieveJobMetaClusterA(id, flag_id, tx)
}

func (a ApiDiscrimPhrases) GetDoc(meta interface{}, tx pgx.Tx) pgUtils.JobDoc {
	return retrieveJobDoc(meta, tx)
}

func (a ApiDiscrimPhrases) GetBodyRequest(job_doc pgUtils.JobDoc, collectionName string, group_id int) api.BodyRequest {
	return api.BodyRequest{
		"title":       job_doc.Title,
		"description": job_doc.Description,
		"meta": map[string]interface{}{
			"groupId": group_id,
			"webId":   job_doc.JobMeta.JobID,
		},
	}
}

func (a ApiDiscrimPhrases) PreSelectDoc() func(pgUtils.JobDoc) bool {
	return nil
}

func (a ApiDiscrimPhrases) GotEnoughInfo(m api.BodyResponse) bool {
	return true
}

func (a ApiDiscrimPhrases) UpdateDatabase(response api.BodyResponse, doc pgUtils.JobDoc, cluster_a_id int, tx pgx.Tx) {
	v, ok := response["phrase_counts"].(map[string]interface{})

	if !ok {
		panic("Error parsing result for discriminatory phrases dict (i.e. something is very wrong)")
	}

	phraseMap := make(map[string]map[string]interface{})
	for categoryName, categoryValue := range v {
		var discrimCategoryName string = ""
		for _, discrimCategory := range DISCRIMINATING_PHRASE_TYPES {
			if strings.HasPrefix(categoryName, discrimCategory+"-") {
				discrimCategoryName = discrimCategory
				break
			}
		}

		if discrimCategoryName == "" {
			continue
		}

		if _, ok := phraseMap[discrimCategoryName]; !ok {
			phraseMap[discrimCategoryName] = make(map[string]interface{})
		}

		phraseMap[discrimCategoryName][categoryName] = categoryValue
	}

	for parentCategoryName, parentCategoryMap := range phraseMap {
		sqlInsertPhrases(parentCategoryMap, cluster_a_id, parentCategoryName, tx)
	}
}
