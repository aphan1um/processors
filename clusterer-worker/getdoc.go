package main

import (
	"fmt"
	pg "go-processors/common/postgres"

	"github.com/jackc/pgtype"
	"github.com/jackc/pgx/v4"
)

func retrieveJobMetaClusterA(id int, flag_id int, tx pgx.Tx) []interface{} {
	return _retrieveJobMetaFromID("cluster_a_id")(id, flag_id, tx)
}

func retrieveJobMetaClusterB(id int, flag_id int, tx pgx.Tx) []interface{} {
	return _retrieveJobMetaFromID("cluster_b_id")(id, flag_id, tx)
}

func _retrieveJobMetaFromID(id_column string) func(int, int, pgx.Tx) []interface{} {
	return func(id int, flag_id int, tx pgx.Tx) []interface{} {
		ret := make([]interface{}, 0)

		var rows pgx.Rows

		if tx == nil {
			rows = pg.QuerySQL(
				fmt.Sprintf(`
				SELECT job_web_id,jobsite_id
				FROM jobs
				WHERE job_id IN (
					SELECT MIN(j.job_id) FROM jobs j
					INNER JOIN "ClusterJobList" ON (j.job_id = "ClusterJobList".job_id)
					WHERE "ClusterJobList".%s = $1
					GROUP BY j.jobsite_id
				)
				ORDER BY jobsite_id ASC`, id_column),
				false, []interface{}{id})
		} else {
			rows = pg.QuerySQLTx(
				fmt.Sprintf(`
				SELECT job_web_id,jobsite_id
				FROM jobs
				WHERE job_id IN (
					SELECT MIN(j.job_id) FROM jobs j
					INNER JOIN "ClusterJobList" ON (j.job_id = "ClusterJobList".job_id)
					WHERE "ClusterJobList".%s = $1
					GROUP BY j.jobsite_id
				)
				ORDER BY jobsite_id ASC`, id_column),
				false, []interface{}{id}, tx)
		}

		defer rows.Close()

		for rows.Next() {
			var job_web_id string
			var jobsite_id int

			if err := rows.Scan(&job_web_id, &jobsite_id); err != nil {
				panic(err)
			}

			ret = append(ret, pg.Job{JobSiteID: jobsite_id, JobID: job_web_id})
		}

		return ret
	}
}

func retrieveJobDoc(meta interface{}, tx pgx.Tx) pg.JobDoc {
	// https://www.alexedwards.net/blog/using-postgresql-jsonb
	var jsonData = make(map[string]interface{})
	docMeta := meta.(pg.Job)

	var err error
	if tx != nil {
		err = pg.QuerySQLRowTx(
			"SELECT data FROM jobs WHERE job_web_id = $1 AND jobsite_id = $2", false,
			[]interface{}{docMeta.JobID, docMeta.JobSiteID}, tx).Scan(&jsonData)
	} else {
		err = pg.QuerySQLRow(
			"SELECT data FROM jobs WHERE job_web_id = $1 AND jobsite_id = $2", false,
			[]interface{}{docMeta.JobID, docMeta.JobSiteID}).Scan(&jsonData)
	}
	if err != nil {
		panic(err)
	}

	jobDoc, err := pg.ToJobDoc(jsonData, docMeta)
	return *jobDoc
}

func retrieveCompanyMeta(id int, flag_id int, tx pgx.Tx) []interface{} {
	var company_name string
	err := pg.QuerySQLRowTx(
		`SELECT company_name
		FROM "Company"
		WHERE company_id = $1`,
		false, []interface{}{id}, tx).Scan(&company_name)
	if err != nil {
		panic(err)
	}

	return []interface{}{company_name}
}

func retrieveCompanyDoc(meta interface{}, tx pgx.Tx) pg.JobDoc {
	return pg.JobDoc{
		Company: meta.(string),
	}
}

func retrieveABNMeta(id int, flag_id int, tx pgx.Tx) []interface{} {
	var company_name string
	var poa_code int
	var abn_id pgtype.Int8
	err := pg.QuerySQLRowTx(
		`SELECT company_name, poa_code, abn_id
		FROM "Company"
		INNER JOIN "CompanyABN" USING (abn_id)
		WHERE company_id = $1`,
		false, []interface{}{id}, tx).Scan(&company_name, &poa_code, &abn_id)
	if err != nil {
		panic(err)
	}

	return []interface{}{map[string]interface{}{
		"company_name": company_name,
		"poa_code":     poa_code,
		"abn_id":       abn_id.Int,
	}}
}

func retrieveABNDoc(meta interface{}, tx pgx.Tx) pg.JobDoc {
	metaMap := meta.(map[string]interface{})
	return pg.JobDoc{
		Company:  metaMap["company_name"].(string),
		Location: metaMap["poa_code"].(int),
		MiscMeta: metaMap["abn_id"].(int64),
	}
}
