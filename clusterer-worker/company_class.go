package main

import (
	"context"
	"strings"

	"github.com/jackc/pgx/v4"
)

func insertCompanyClass(class string, tx pgx.Tx) int {
	classNormalised := strings.ToLower(strings.TrimSpace(class))

	// add company classification
	_, err := tx.Exec(
		context.Background(),
		`INSERT INTO "CompanyClass" (classification) VALUES ($1) ON CONFLICT (classification) DO NOTHING`,
		classNormalised)
	if err != nil {
		panic(err)
	}

	// now get its id
	var class_id int
	err = tx.QueryRow(
		context.Background(),
		`SELECT class_id FROM "CompanyClass" WHERE classification = $1`,
		classNormalised).Scan(&class_id)
	if err != nil {
		panic(err)
	}

	return class_id
}
