package main

import (
	"context"
	"go-processors/common/api"
	pgUtils "go-processors/common/postgres"

	"github.com/jackc/pgx/v4"
)

type ApiExperience struct{}

func (a ApiExperience) FlagName() string {
	return "FLAG_EXPERIENCE"
}

func (a ApiExperience) Cluster() byte {
	return 'B'
}

func (a ApiExperience) ApiPath() string {
	return "/job/experience"
}

func (a ApiExperience) Server() api.ApiServer {
	return api.ServerJobs
}

func (a ApiExperience) GetDocList(id int, flag_id int, tx pgx.Tx) []interface{} {
	return retrieveJobMetaClusterB(id, flag_id, tx)
}

func (a ApiExperience) GetDoc(meta interface{}, tx pgx.Tx) pgUtils.JobDoc {
	return retrieveJobDoc(meta, tx)
}

func (a ApiExperience) GetBodyRequest(job_doc pgUtils.JobDoc, collectionName string, group_id int) api.BodyRequest {
	return api.BodyRequest{
		"title":       job_doc.Title,
		"description": job_doc.Description,
		"meta": map[string]interface{}{
			"groupId": group_id,
			"webId":   job_doc.JobMeta.JobID,
		},
	}
}

func (a ApiExperience) PreSelectDoc() func(pgUtils.JobDoc) bool {
	return nil
}

func (a ApiExperience) GotEnoughInfo(m api.BodyResponse) bool {
	return m["education_level"] != nil
}

func (a ApiExperience) UpdateDatabase(response api.BodyResponse, doc pgUtils.JobDoc, cluster_b_id int, tx pgx.Tx) {
	var education_level_mapped *int

	if response["education_level"] == nil {
		education_level_mapped = nil
	} else {
		education_level_mapped = new(int)
		*education_level_mapped = postgresEducationLevelMap[response["education_level"].(string)]
	}

	_, err := tx.Exec(
		context.Background(),
		`INSERT INTO "ClusterBEducation" (cluster_b_id, edulevel_id) VALUES ($1, $2)`,
		cluster_b_id,
		education_level_mapped)

	if err != nil {
		panic(err)
	}

	if response["experience_years"] != nil {
		_, err = tx.Exec(
			context.Background(),
			`INSERT INTO "ClusterBExperience" (cluster_b_id, years_needed) VALUES ($1, $2)`,
			cluster_b_id,
			response["experience_years"].(float64))

		if err != nil {
			panic(err)
		}
	}
}
