package main

import (
	"context"
	"go-processors/common/api"
	pgUtils "go-processors/common/postgres"

	"github.com/jackc/pgx/v4"
)

type ApiDnb struct{}

func (a ApiDnb) FlagName() string {
	return "FLAG_DNB"
}

func (a ApiDnb) Cluster() byte {
	return 'C'
}

func (a ApiDnb) ApiPath() string {
	return "/dnb/with-postcode"
}

func (a ApiDnb) Server() api.ApiServer {
	return api.ServerGeo
}

func (a ApiDnb) GetDocList(id int, flag_id int, tx pgx.Tx) []interface{} {
	return retrieveABNMeta(id, flag_id, tx)
}

func (a ApiDnb) GetDoc(meta interface{}, tx pgx.Tx) pgUtils.JobDoc {
	return retrieveABNDoc(meta, tx)
}

func (a ApiDnb) GetBodyRequest(doc pgUtils.JobDoc, collectionName string, company_id int) api.BodyRequest {
	return api.BodyRequest{
		"query":    doc.Company,
		"postcode": doc.Location.(int),
	}
}

func (a ApiDnb) PreSelectDoc() func(pgUtils.JobDoc) bool {
	return nil
}

func (a ApiDnb) GotEnoughInfo(m api.BodyResponse) bool {
	return true
}

func (a ApiDnb) UpdateDatabase(response api.BodyResponse, doc pgUtils.JobDoc, company_id int, tx pgx.Tx) {
	if len(response["companiesFound"].([]interface{})) > 0 && response["chosenResult"] != nil {
		// pgUtils.ExecuteSQLTx(
		// 	"INSERT INTO cache_companies_anzsic (company_id, source_id, content) VALUES ($1, $2, $3)",
		// 	true,
		// 	[]interface{}{company_id, postgresAnzsicSource["dnb"], val_dnb},
		// 	tx)

		for _, class := range response["chosenResult"].(map[string]interface{})["industry"].([]interface{}) {
			class_id := insertCompanyClass(class.(string), tx)
			tx.Exec(
				context.Background(),
				`INSERT INTO "CompanyDNBClassification" (abn_id, class_id) VALUES ($1, $2)`,
				doc.MiscMeta.(int64), class_id)
		}
	}
}
