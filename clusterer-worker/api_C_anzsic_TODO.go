package main

import (
	"context"
	"fmt"
	"go-processors/common/api"
	pgUtils "go-processors/common/postgres"
	"log"
	"strconv"

	"github.com/jackc/pgx/v4"
)

type ApiAnzsic struct{}

func (a ApiAnzsic) FlagName() string {
	return "FLAG_ANZSIC"
}

func (a ApiAnzsic) Cluster() byte {
	return 'C'
}

func (a ApiAnzsic) ApiPath() string {
	return "/anzsic"
}

func (a ApiAnzsic) Server() api.ApiServer {
	return api.ServerGeo
}

func (a ApiAnzsic) GetDocList(id int, flag_id int, tx pgx.Tx) []interface{} {
	return retrieveCompanyMeta(id, flag_id, tx)
}

func (a ApiAnzsic) GetDoc(meta interface{}, tx pgx.Tx) pgUtils.JobDoc {
	return retrieveCompanyDoc(meta, tx)
}

func (a ApiAnzsic) GetBodyRequest(job_doc pgUtils.JobDoc, collectionName string, group_id int) api.BodyRequest {
	// TODO: Add location
	return api.BodyRequest{
		"query":    job_doc.Company,
		"location": "",
	}
}

func (a ApiAnzsic) PreSelectDoc(doc pgUtils.JobDoc) bool {
	return true
}

func (a ApiAnzsic) GotEnoughInfo(m api.BodyResponse) bool {
	return true
}

func (a ApiAnzsic) UpdateDatabase(response api.BodyResponse, company_id int, tx pgx.Tx) {
	switch response["debug"].(map[string]interface{})["externals"].(map[string]interface{})["abr"].(map[string]interface{})["status"].(string) {
	case "success":
		var anzsic_code_div_ptr *string
		var anzsic_code_number_ptr *int

		if response["anzsic"].(map[string]interface{})["result"] != nil {
			anzsic_code_full := response["anzsic"].(map[string]interface{})["result"].(string)
			anzsic_code_div := anzsic_code_full[:1]
			anzsic_code_div_ptr = &anzsic_code_div

			if len(anzsic_code_full) > 1 {
				anzsic_code_number, err := strconv.Atoi(anzsic_code_full[1:])
				anzsic_code_number_ptr = &anzsic_code_number
				if err != nil {
					panic(err)
				}
			}

			_, err := tx.Exec(
				context.Background(),
				`UPDATE \"Company\" SET anzsic_id = $1, anzsic_division = $2 WHERE company_id = $3`,
				anzsic_code_number_ptr,
				anzsic_code_div_ptr,
				company_id)
			if err != nil {
				panic(err)
			}
		}

	// TODO: Improve non-successful ANZSIC calls
	case "noresult":
		fmt.Printf("[WARN] Unable to find any ABN with name that could confirm its firm type for company ID: '%s'\n", company_id)
	case "failure":
		log.Fatalln("Unable to find any ABN with name that could confirm its firm type")
	}
}
