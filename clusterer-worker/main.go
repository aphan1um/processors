package main

import (
	"context"
	"errors"
	"fmt"
	"go-processors/common/api"
	"runtime"
	"time"

	pg "go-processors/common/postgres"

	"github.com/jackc/pgx/v4"
)

const REPOLL_SECONDS = 60
const MAX_PARALLEL_WORKERS = 4

var worker_chan = make(chan struct{}, MAX_PARALLEL_WORKERS)

var TableMappings = map[byte][]string{
	'A': {"ClusterAQueue", "ClusterA", "cluster_a_id"},
	'B': {"ClusterBQueue", "ClusterB", "cluster_b_id"},
	'C': {"CompanyQueue", "Company", "company_id"},
}

var TableFlagMappings = map[byte]*map[string]int{
	'A': &postgresClusterAFlagMap,
	'B': &postgresClusterBFlagMap,
	'C': &postgresClusterCFlagMap,
}

var ALL_WORKS = []api.ApiOperation{
	// Cluster A
	ApiAnzsco{},
	ApiDiscrimPhrases{},
	ApiSoftSkills{},
	// Cluster B
	ApiAge{},
	ApiExperience{},
	ApiWorkType{},
	// Cluster C (Company)
	ApiAbn{},
	ApiDnb{},
}

func main() {
	pg.ConnectToPostgres()

	retrieveDataTables()
	fmt.Println("[DEBUG] Data tables loaded")

	loadProfile()
	fmt.Println("[DEBUG] Profile loaded")

	for _, task := range ALL_WORKS {
		go func(apiOp api.ApiOperation) {
			fmt.Printf("[DEBUG] Started up worker %c %s\n", apiOp.Cluster(), apiOp.FlagName())

			for {
				worker_chan <- struct{}{}

				item_id, tx, err := startTransaction(apiOp.Cluster(), apiOp.FlagName())
				if errors.Is(err, pgx.ErrNoRows) {
					fmt.Printf("[DEBUG] No task item found for %c %s, sleeping for %d secs\n", apiOp.Cluster(), apiOp.FlagName(), REPOLL_SECONDS)
					err = tx.Commit(context.Background())
					if err != nil {
						panic(err)
					}

					<-worker_chan
					time.Sleep(time.Duration(REPOLL_SECONDS) * time.Second)
				} else if err != nil {
					panic(err)
				} else {
					flag_id := (*TableFlagMappings[apiOp.Cluster()])[apiOp.FlagName()]
					itemList := apiOp.GetDocList(item_id, flag_id, tx)
					fmt.Printf("[INFO] Got list for %c %s: %#v\n", apiOp.Cluster(), apiOp.FlagName(), itemList)

					processItemList(apiOp, itemList, item_id, tx)
					removeTaskFromQueue(apiOp.Cluster(), item_id, flag_id, tx)

					if err := tx.Commit(context.Background()); err != nil {
						panic(err)
					}

					fmt.Printf("[INFO] Finished work for %c %s with item ID: %d\n", apiOp.Cluster(), apiOp.FlagName(), item_id)
					<-worker_chan
				}
			}
		}(task)
	}

	runtime.Goexit()

	fmt.Println("[DEBUG] Exiting program...")
}

func startTransaction(cluster byte, flag_name string) (int, pgx.Tx, error) {
	var item_id int

	tx, err := pg.Client.BeginTx(context.Background(), pgx.TxOptions{})
	if err != nil {
		panic(err)
	}

	err = pg.QuerySQLRowTx(
		getQueueQuery(cluster, flag_name),
		false,
		[]interface{}{}, tx).Scan(&item_id)

	return item_id, tx, err
}

func processItemList(apiOp api.ApiOperation, list []interface{}, item_id int, tx pgx.Tx) {
	var prevResponse map[string]interface{}
	var prevJobDoc pg.JobDoc
	var preselectedJobDoc *pg.JobDoc = nil

	if apiOp.PreSelectDoc() != nil {
		for _, job := range list {
			job_doc := apiOp.GetDoc(job, tx)
			if apiOp.PreSelectDoc()(job_doc) {
				preselectedJobDoc = &job_doc
				break
			}
		}
	}

	if preselectedJobDoc != nil {
		job_doc := *preselectedJobDoc

		response := api.CallAPI(
			apiOp, job_doc, item_id,
			map[string]interface{}{"item_id": item_id, "preselected": true},
			true)

		apiOp.UpdateDatabase(response, job_doc, item_id, tx)
	} else {
		for i, job := range list {
			// fmt.Printf("> Getting job document '%s' from site: '%s'\n", job.JobID, pg.JobSiteMap[job.JobSiteID])
			job_doc := apiOp.GetDoc(job, tx)

			response := api.CallAPI(
				apiOp, job_doc, item_id,
				map[string]interface{}{"item_id": item_id, "iteration": i},
				true)

			if apiOp.GotEnoughInfo(response) {
				apiOp.UpdateDatabase(response, job_doc, item_id, tx)
				break
			} else {
				prevResponse = response
				prevJobDoc = job_doc
			}

			if len(list) == i+1 {
				apiOp.UpdateDatabase(prevResponse, prevJobDoc, item_id, tx)
				break
			} else {
				fmt.Printf("[TRACE] [processJobList] Item id %d: %s Didn't get enough info\n", item_id, apiOp.ApiPath())
			}
		}
	}

	fmt.Printf("[TRACE] [processJobList] Item id %d: %s finalising flag value\n", item_id, apiOp.ApiPath())
	// update flag for job group that the feature has been processed
	_, err := tx.Exec(
		context.Background(),
		getFlagUpdateQuery(apiOp.Cluster()),
		item_id, 1<<(*TableFlagMappings[apiOp.Cluster()])[apiOp.FlagName()])
	if err != nil {
		panic(err)
	}

	fmt.Printf("[TRACE] [END] Item id %d: %s\n", item_id, apiOp.ApiPath())
}

func removeTaskFromQueue(cluster byte, item_id int, flag_id int, tx pgx.Tx) {
	query_to_exec := fmt.Sprintf(
		`DELETE FROM "%[1]s" WHERE %[2]s = $1 AND flag_id = $2`,
		TableMappings[cluster][0], TableMappings[cluster][2])

	_, err := tx.Exec(context.Background(), query_to_exec, item_id, flag_id)
	if err != nil {
		panic(err)
	}
}

func getQueueQuery(cluster byte, flag_name string) string {
	return fmt.Sprintf(
		`SELECT %[1]s FROM "%[2]s" WHERE flag_id = %[3]d LIMIT 1 FOR SHARE OF "%[2]s" SKIP LOCKED`,
		TableMappings[cluster][2], TableMappings[cluster][0], (*TableFlagMappings[cluster])[flag_name])
}

func getFlagUpdateQuery(cluster byte) string {
	return fmt.Sprintf(
		`UPDATE "%[2]s" SET flag = flag | $1 WHERE %[1]s = $2`,
		TableMappings[cluster][2], TableMappings[cluster][1])
}
