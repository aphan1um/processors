package main

import (
	common "go-processors/common"
	"strconv"
)

var CONFIG_PROFILE = common.GetEnv("CONFIG_PROFILE", "")

var JOBS_API_URL = common.GetEnv("JOBS_API_URL", "http://localhost:8000")
var DELAY_LOOP_MS, _ = strconv.Atoi(common.GetEnv("DELAY_LOOP_MS", "500"))
var DEBUG_MODE = len(common.GetEnv("DEBUG_MODE", "")) > 0

var CRITERIA_DUPLICATE_THRESHOLD float64
var TRIGRAM_SIMILARITY_THRESHOLD float64
