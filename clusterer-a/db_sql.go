package main

import (
	"errors"
	"fmt"
	"strings"

	pg "go-processors/common/postgres"

	"github.com/jackc/pgx/v4"
)

func addAdvertsToJobGroup(entries []pg.Job, trueDoc *pg.Job, companyName string, tx pgx.Tx) {
	var group_id int
	var job_id int

	if trueDoc == nil {
		group_id = createJobGroupID(companyName, tx)
	} else {
		err := pg.QuerySQLRowTx(
			"SELECT job_id FROM jobs WHERE jobsite_id = $1 AND job_web_id = $2",
			true,
			[]interface{}{trueDoc.JobSiteID, trueDoc.JobID}, tx).Scan(&job_id)
		if err != nil {
			panic(err)
		}

		// get the job group number
		err = pg.QuerySQLRowTx(
			`SELECT "ClusterA".cluster_a_id AS group_id
			FROM "ClusterJobList"
			INNER JOIN "ClusterA" ON ("ClusterA".cluster_a_id = "ClusterJobList".cluster_a_id)
			WHERE "ClusterJobList".job_id = $1 AND "ClusterA".config_id = $2`,
			true, []interface{}{job_id, CONFIG_PROFILE}, tx).Scan(&group_id)

		if err != nil {
			panic(err)
		}
	}

	for _, entry := range entries {
		// get job ID from entry
		pg.QuerySQLRowTx(
			"SELECT job_id FROM jobs WHERE jobsite_id = $1 AND job_web_id = $2",
			true,
			[]interface{}{entry.JobSiteID, entry.JobID}, tx).Scan(&job_id)

		// insert job advert into a job group
		pg.ExecuteSQLTx(
			"INSERT INTO \"ClusterJobList\" (cluster_a_id, job_id) VALUES ($1, $2)",
			true,
			[]interface{}{group_id, job_id},
			tx)
	}

	if len(entries) > 1 {
		fmt.Printf("! Added %d jobs to job group %d\n", len(entries), group_id)
	}
}

func getCompanyID(companyName string, tx pgx.Tx) int {
	var company_id int

	// make company name case insensitive
	companyNameProcesed := strings.TrimSpace(strings.ToLower(companyName))

	fmt.Printf("[Company] Company: '%s' -> '%s'\n", companyName, companyNameProcesed)

	err := pg.QuerySQLRowTx("SELECT company_id FROM \"Company\" WHERE company_name = $1", true, []interface{}{companyNameProcesed}, tx).Scan(&company_id)

	if errors.Is(err, pgx.ErrNoRows) {
		err = pg.QuerySQLRowTx("INSERT INTO \"Company\" (company_name) VALUES ($1) RETURNING company_id", true, []interface{}{companyNameProcesed}, tx).Scan(&company_id)
	}

	if err != nil {
		panic(err)
	}

	fmt.Printf("[Company] Retrieved company ID for company '%s': %d\n", companyNameProcesed, company_id)
	return company_id
}

func createJobGroupID(companyName string, tx pgx.Tx) int {
	var newId int
	company_id := getCompanyID(companyName, tx)
	err := pg.QuerySQLRowTx(
		"INSERT INTO \"ClusterA\" (config_id, company_id) VALUES ($1, $2) RETURNING cluster_a_id",
		true,
		[]interface{}{CONFIG_PROFILE, company_id}, tx).Scan(&newId)

	if err != nil {
		panic(err)
	}

	return newId
}

func GetRandomDocument(tx pgx.Tx) pg.JobDoc {
	var job_id int
	var job_web_id string
	var jobsite_id int

	fmt.Println("[DEBUG] Now getting random document")

	err := pg.QuerySQLRowTx(
		`SELECT jobs.job_id AS job_id,jobs.job_web_id AS job_web_id,jobs.jobsite_id AS jobsite_id
		FROM (
			SELECT * FROM "ClusterJobList"
			INNER JOIN (SELECT * FROM "ClusterA" WHERE config_id = $1) t1 ON ("ClusterJobList".cluster_a_id = t1.cluster_a_id)
			WHERE "ClusterJobList".job_id NOT IN (SELECT job_id FROM jobscleanererrors WHERE config_id = $1)
		) o1
		RIGHT JOIN jobs ON (o1.job_id = jobs.job_id)
		WHERE (o1.config_id IS NULL OR o1.config_id <> $1)
		LIMIT 1`,
		true, []interface{}{CONFIG_PROFILE}, tx).Scan(&job_id, &job_web_id, &jobsite_id)
	if err != nil {
		panic(err)
	}

	ret, _ := pg.GetJobDocument(pg.Job{JobID: job_web_id, JobSiteID: jobsite_id}, true, tx)
	fmt.Printf("[DEBUG] Got random doc (from site %s) with job_id: %v\n", pg.JobSiteMap[jobsite_id], job_web_id)

	return ret
}

func MarkJobDocAsInvalid(job_id int, err error) {
	pg.ExecuteSQL(
		"INSERT INTO jobscleanererrors (job_id, config_id, message) VALUES ($1, $2, $3) ON CONFLICT (job_id, config_id) DO UPDATE SET message = $3",
		true,
		[]interface{}{job_id, CONFIG_PROFILE, err.Error()})
}
