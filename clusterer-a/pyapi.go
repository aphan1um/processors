package main

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
)

func getCompanyNameByDescription(description string) string {
	bodyRequest, err := json.Marshal(map[string]interface{}{
		"text": description,
	})

	if err != nil {
		panic(err)
	}

	resp, err := http.Post(JOBS_API_URL+"/other/company", "application/json", bytes.NewBuffer(bodyRequest))
	if err != nil {
		panic(err)
	}

	body, _ := ioutil.ReadAll(resp.Body)
	var rawMap map[string]interface{}
	json.Unmarshal(body, &rawMap)

	if err != nil {
		panic(err)
	}

	return rawMap["company_name"].(string)
}
