package main

import (
	"context"
	"fmt"
	"math"
	"strconv"
	"strings"
	"time"
	"unicode/utf8"

	levenshtein "github.com/arbovm/levenshtein"
	"github.com/jackc/pgx/v4"
	fast_levenshtein "github.com/ka-weihe/fast-levenshtein"

	pg "go-processors/common/postgres"
)

func main() {
	pg.ConnectToPostgres()
	loadProfile()
	// defer pg.Client.Close(context.Background())

	fmt.Printf("[DEBUG] Is debug mode enabled: %v\n", DEBUG_MODE)
	fmt.Printf("[DEBUG] Job API URL: %s\n", JOBS_API_URL)
	fmt.Printf("[DEBUG] TRIGRAM_SIMILARITY_THRESHOLD = %f\n", TRIGRAM_SIMILARITY_THRESHOLD)
	fmt.Printf("[DEBUG] CRITERIA_DUPLICATE_THRESHOLD = %f\n", CRITERIA_DUPLICATE_THRESHOLD)

	for {
		tx, err := pg.Client.Begin(context.Background())
		if err != nil {
			panic(err)
		}

		// set similarity of trigrams
		pg.ExecuteSQLTx(
			fmt.Sprintf("SET pg_trgm.similarity_threshold = %f", TRIGRAM_SIMILARITY_THRESHOLD),
			true, []interface{}{},
			tx)

		chosenDocument := GetRandomDocument(tx)
		duplicateDocs, trueDoc, companyName := findDuplicateAds(chosenDocument, tx)

		// add the chosen documents and its duplicates to database
		addAdvertsToJobGroup(duplicateDocs, trueDoc, companyName, tx)

		// commit transaction for job group and job documents in this group
		if err = tx.Commit(context.Background()); err != nil {
			panic(err)
		}

		// TODO: DEBUG
		time.Sleep(time.Duration(DELAY_LOOP_MS) * time.Millisecond)
	}
}

func findDuplicateAds(originalDocument pg.JobDoc, tx pgx.Tx) (duplicateDocs []pg.Job, trueDoc *pg.Job, companyName string) {
	companyName = originalDocument.Company
	searchForDuplicateDocs := true

	// add original doc to the ("soon to be" created) job group
	duplicateDocs = append(duplicateDocs, originalDocument.JobMeta)

	// try to get company name via description, if not explicitly specified
	if len(strings.TrimSpace(companyName)) == 0 {
		fmt.Println("[INFO] Company name empty, try to find via description")
		companyName = getCompanyNameByDescription(originalDocument.Description)

		if len(companyName) == 0 {
			searchForDuplicateDocs = false
			fmt.Println("[WARN] Could not find company name for this document...")
		} else {
			fmt.Printf("[INFO] Company name found: %s\n", companyName)
			originalDocument.Company = companyName
		}
	}

	if searchForDuplicateDocs {
		// get other jobs with a 'similar' title and strictly the same company name
		rows := pg.QuerySQLTx(
			`SELECT job_id, jobsite_id, job_web_id, data, similarity("data"->>'title', $1) AS title_sim
			FROM jobs
			WHERE
				trim("data"->>'title') % $1 AND
				trim(lower((data->>'company'))) = trim(lower($2)) AND
				jobs.job_id NOT IN (SELECT job_id FROM jobscleanererrors WHERE config_id = $3) AND NOT
				(jobsite_id = $4 AND job_web_id = $5)`,
			true, []interface{}{originalDocument.Title, originalDocument.Company, CONFIG_PROFILE, originalDocument.JobMeta.JobSiteID, originalDocument.JobMeta.JobID}, tx)

		// create Job structs for each possible job duplicate
		resultsList := []pg.JobDoc{}
		i_loop := 0
		time_start := time.Now()

		for rows.Next() {
			var job_id int
			var jobsite_id int
			var job_web_id string
			var data map[string]interface{}
			var title_sim float64

			i_loop += 1

			if err := rows.Scan(&job_id, &jobsite_id, &job_web_id, &data, &title_sim); err != nil {
				panic(err)
			}

			job_doc, err := pg.ToJobDoc(data, pg.Job{JobID: job_web_id, JobSiteID: jobsite_id})

			if err != nil { // if there's something wrong with the job advert (e.g. empty/invalid date time)
				MarkJobDocAsInvalid(job_id, err)
				fmt.Printf("[DEBUG] [SEARCH] (%v, %d, title sim: %f) Marked job_id %v as invalid\n", time.Since(time_start), i_loop, title_sim, job_id)
			} else {
				resultsList = append(resultsList, *job_doc)
				fmt.Printf("[DEBUG] [SEARCH] (%v, %d, title sim: %f) Added job_id %v to list of potent duplicates\n", time.Since(time_start), i_loop, title_sim, job_id)
			}

			time_start = time.Now()
		}

		rows.Close()

		fmt.Printf("[DEBUG] Amount of potential duplicates found: %d\n", len(resultsList))
		for i, i_doc := range resultsList {

			score := stringSimilarity(originalDocument.Description, i_doc.Description)
			isDuplicate := score >= CRITERIA_DUPLICATE_THRESHOLD

			// check if potential duplicate is already in a job group
			var i_doc_already_in_jobgroup bool
			err := pg.QuerySQLRowTx(
				`SELECT EXISTS (
					SELECT *
					FROM "ClusterA"
					INNER JOIN "ClusterJobList"
					ON ("ClusterA".cluster_a_id = "ClusterJobList".cluster_a_id)
					WHERE
						"ClusterJobList".job_id = (SELECT job_id FROM jobs WHERE jobsite_id = $1 AND job_web_id = $2) AND
						"ClusterA".config_id = $3
				)`,
				false, []interface{}{i_doc.JobMeta.JobSiteID, i_doc.JobMeta.JobID, CONFIG_PROFILE}, tx).Scan(&i_doc_already_in_jobgroup)

			if err != nil {
				panic(err)
			}

			fmt.Printf("[DEBUG] [FILTER] [%s] (%d/%d, config: %s, alreadyInGroup: %v) Score: %.2f %v\n",
				strings.ToUpper(strconv.FormatBool(isDuplicate)), i+1, len(resultsList), CONFIG_PROFILE, i_doc_already_in_jobgroup,
				score, i_doc.JobMeta.JobID)

			if isDuplicate && i_doc_already_in_jobgroup {
				fmt.Printf("[WARN] [FILTER] (%d/%d) Original document found to be duplicate of another flagged doc!\n", i+1, len(resultsList))
				trueDoc = &i_doc.JobMeta
				break
			} else if isDuplicate && !i_doc_already_in_jobgroup { // only add duplicate if not in group
				duplicateDocs = append(duplicateDocs, i_doc.JobMeta)
			}
		}
	}

	return
}

func stringSimilarity(textA string, textB string) (distance float64) {
	maxLen := math.Max(float64(utf8.RuneCountInString(textA)), float64(utf8.RuneCountInString(textB)))

	defer func() {
		if err := recover(); err != nil {
			distance = 1 - float64(levenshtein.Distance(textA, textB))/maxLen
		}
	}()

	// normalise
	distance = 1 - float64(fast_levenshtein.Distance(textA, textB))/maxLen
	return
}

func loadProfile() {
	var profile = make(map[string]interface{})

	if len(CONFIG_PROFILE) == 0 {
		panic("CONFIG_PROFILE is empty or not provided")
	}

	err := pg.Client.QueryRow(context.Background(), "SELECT meta FROM \"ClusterConfig\" WHERE config_id = $1", CONFIG_PROFILE).Scan(&profile)
	if err != nil {
		panic(err)
	}

	CRITERIA_DUPLICATE_THRESHOLD = profile["duplicate_threshold"].(float64)
	TRIGRAM_SIMILARITY_THRESHOLD = profile["trigram_similarity_threshold"].(float64)
}
