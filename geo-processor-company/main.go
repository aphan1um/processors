package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"time"

	pg "go-processors/common/postgres"
)

func main() {
	pg.ConnectToPostgres()

	retrieveDataTables()
	fmt.Println("[DEBUG] All data tables retrieved")

	for { // loop
		tx, err := pg.Client.Begin(context.Background())
		if err != nil {
			panic(err)
		}

		companyName, companyId := getCompanyName(tx)

		// TODO: LOCATION
		bodyRequest, _ := json.Marshal(map[string]interface{}{
			"query":    companyName,
			"location": "",
		})

		fmt.Println("=========================================")
		fmt.Printf("> Company name to process: %s\n", companyName)

		resp, err := http.Post(GEO_API_URL+"/anzsic", "application/json", bytes.NewBuffer(bodyRequest))
		if err != nil {
			panic(err)
		}

		body, _ := ioutil.ReadAll(resp.Body)
		var rawMap map[string]interface{}
		json.Unmarshal(body, &rawMap)

		if err != nil {
			panic(err)
		}

		fmt.Println(string(body))

		switch rawMap["debug"].(map[string]interface{})["externals"].(map[string]interface{})["abr"].(map[string]interface{})["status"].(string) {
		case "success":
			entity_type := rawMap["entity_type"].(string)
			var anzsic_code_div_ptr *string
			var anzsic_code_number_ptr *int
			var employee_count *int

			if rawMap["anzsic"].(map[string]interface{})["result"] != nil {
				anzsic_code_full := rawMap["anzsic"].(map[string]interface{})["result"].(string)
				anzsic_code_div := anzsic_code_full[:1]
				anzsic_code_div_ptr = &anzsic_code_div

				if len(anzsic_code_full) > 1 {
					anzsic_code_number, err := strconv.Atoi(anzsic_code_full[1:])
					anzsic_code_number_ptr = &anzsic_code_number
					if err != nil {
						panic(err)
					}
				}
			}

			if rawMap["employee"] != nil && rawMap["employee"].(map[string]interface{})["employees_num"] != nil {
				tmp_count := int(rawMap["employee"].(map[string]interface{})["employees_num"].(float64))
				employee_count = &tmp_count
			}

			sqlUpdateCompany(companyName, anzsic_code_number_ptr, anzsic_code_div_ptr, &entity_type, employee_count, tx)
			sqlUpdateCache(companyId, rawMap, tx)
		case "noresult":
			{
				log.Printf("[WARN] Unable to find any ABN with name that could confirm its firm type for company: '%s'\n", companyName)
				sqlUpdateCompany(companyName, nil, nil, nil, nil, tx)
			}
		case "failure":
			log.Fatalln("Unable to find any ABN with name that could confirm its firm type")
		}

		if err = tx.Commit(context.Background()); err != nil {
			panic(err)
		}

		time.Sleep(time.Duration(DELAY_LOOP_MS) * time.Millisecond)
	}
}
