package main

import (
	common "go-processors/common"
	"strconv"
)

var DELAY_LOOP_MS, _ = strconv.Atoi(common.GetEnv("DELAY_LOOP_MS", "2000"))
var DELAY_LOOP_NO_ITEMS_MS, _ = strconv.Atoi(common.GetEnv("DELAY_LOOP_NO_ITEMS_MS", "10000"))
var GEO_API_URL = common.GetEnv("GEO_API_URL", "http://localhost:3000")

var PROCESSOR_DATA_VERSION, _ = strconv.Atoi(common.GetEnv("PROCESSOR_DATA_VERSION", "0"))
