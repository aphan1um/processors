package main

import (
	"context"
	"errors"
	"fmt"
	"log"
	"strings"
	"time"

	pg "go-processors/common/postgres"

	"github.com/jackc/pgx/v4"
)

func getCompanyName(tx pgx.Tx) (string, int) {
	var company_name string
	var company_id int
	// length > 0 avoids the empty string (representing the jobs that don't have it explicitly specified)
	fGetName := func() error {
		return pg.QuerySQLRow(
			`SELECT company_id, company_name FROM "Company"
			WHERE (LEFT(entity_id, 1) <> '_' OR entity_id IS NULL) AND version < $1
			LIMIT 1`,
			true, []interface{}{PROCESSOR_DATA_VERSION}).Scan(&company_id, &company_name)
	}
	err := fGetName()

	for err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			fmt.Printf("[WARN] No unprocessed company names to work with, wait for %d ms...\n", DELAY_LOOP_NO_ITEMS_MS)
			time.Sleep(time.Duration(DELAY_LOOP_NO_ITEMS_MS) * time.Millisecond)
		} else {
			log.Fatal(err)
		}

		err = fGetName()
	}

	return company_name, company_id
}

func sqlUpdateCompany(company_name string, anzsic_id *int, anzsic_division *string, entity_id *string, employee_count *int, tx pgx.Tx) {
	pg.ExecuteSQLTx(
		"UPDATE \"Company\" SET entity_id = $1, version = $2, anzsic_id = $3, anzsic_division = $4, employee_count = $5 WHERE company_name = $6",
		true,
		[]interface{}{entity_id, PROCESSOR_DATA_VERSION, anzsic_id, anzsic_division, employee_count, company_name}, tx)
}

func sqlUpdateCache(company_id int, resp map[string]interface{}, tx pgx.Tx) {
	externals := resp["debug"].(map[string]interface{})["externals"].(map[string]interface{})

	// check if dnb was accessed
	if val_dnb, ok := externals["dunbrad"].(map[string]interface{}); ok && len(val_dnb["companiesFound"].([]interface{})) > 0 && val_dnb["chosenResult"] != nil {
		pg.ExecuteSQLTx(
			"INSERT INTO cache_companies_anzsic (company_id, source_id, content) VALUES ($1, $2, $3)",
			true,
			[]interface{}{company_id, postgresAnzsicSource["dnb"], val_dnb},
			tx)

		for _, class := range val_dnb["chosenResult"].(map[string]interface{})["industry"].([]interface{}) {
			_, err := tx.Exec(
				context.Background(),
				"INSERT INTO cache_companies_class (classification) VALUES ($1) ON CONFLICT (classification) DO NOTHING",
				strings.ToLower(strings.TrimSpace(class.(string))))

			if err != nil {
				panic(err)
			}
		}
	}

	// check if google was accessed
	if val_google, ok_google := externals["google"].(map[string]interface{}); ok_google {

		pg.ExecuteSQLTx(
			"INSERT INTO cache_companies_anzsic (company_id, source_id, content) VALUES ($1, $2, $3)",
			true,
			[]interface{}{company_id, postgresAnzsicSource["google"], val_google},
			tx)

		if type_google, ok_type := val_google["type"].(string); ok_type && len(strings.ToLower(strings.TrimSpace(type_google))) > 0 {
			_, err := tx.Exec(
				context.Background(),
				"INSERT INTO cache_companies_class (classification) VALUES ($1) ON CONFLICT (classification) DO NOTHING",
				strings.ToLower(strings.TrimSpace(type_google)))

			if err != nil {
				panic(err)
			}
		}
	}
}
