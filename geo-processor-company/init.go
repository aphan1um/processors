package main

import (
	"context"
	pg "go-processors/common/postgres"
)

var postgresAnzsicSource = make(map[string]int)

func retrieveDataTables() {
	_retrieveAnzsicSources()
}

func _retrieveAnzsicSources() {
	rows, _ := pg.Client.Query(context.Background(), "SELECT source_id,source_name FROM infoanzsicsource")
	defer rows.Close()
	for rows.Next() {
		var (
			source_id   int
			source_name string
		)
		rows.Scan(&source_id, &source_name)
		postgresAnzsicSource[source_name] = source_id
	}
}
