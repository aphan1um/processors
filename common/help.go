package common

import (
	"fmt"
	"os"
	"time"
)

func GetEnv(key string, defaultValue string) string {
	if len(os.Getenv(key)) > 0 {
		return os.Getenv(key)
	} else {
		return defaultValue
	}
}

func Elapsed(what string) func() {
	start := time.Now()
	return func() {
		fmt.Printf("[%v] %s\n", time.Since(start), what)
	}
}
