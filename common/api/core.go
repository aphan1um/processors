package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"go-processors/common/postgres"
	"io"
	"io/ioutil"
	"net/http"

	pgUtils "go-processors/common/postgres"

	"github.com/jackc/pgx/v4"
)

type BodyRequest map[string]interface{}
type BodyResponse map[string]interface{}

type ApiOperation interface {
	FlagName() string
	Cluster() byte
	ApiPath() string
	Server() ApiServer
	GetDocList(int, int, pgx.Tx) []interface{}
	GetDoc(interface{}, pgx.Tx) postgres.JobDoc
	PreSelectDoc() func(postgres.JobDoc) bool
	GetBodyRequest(postgres.JobDoc, string, int) BodyRequest
	GotEnoughInfo(BodyResponse) bool
	UpdateDatabase(BodyResponse, postgres.JobDoc, int, pgx.Tx)
}

type ApiServer struct {
	Host         string
	PrintRequest func(*http.Response, string, map[string]interface{}, map[string]interface{}, map[string]interface{}) map[string]interface{}
}

func CallAPI(api ApiOperation, job_doc pgUtils.JobDoc, group_id int, meta map[string]interface{}, printRequestBody bool) BodyResponse {
	fmt.Printf("[TRACE] [REQUEST] Group id %d: %s\n", group_id, api.ApiPath())

	bodyRequest := api.GetBodyRequest(job_doc, pgUtils.JobSiteMap[job_doc.JobMeta.JobSiteID], group_id)
	bodyRequestJson := mapToJsonString(bodyRequest)

	resp, err := http.Post(api.Server().Host+api.ApiPath(), "application/json", bytes.NewBuffer(bodyRequestJson))
	if err != nil {
		if printRequestBody {
			j_req, err := json.Marshal(bodyRequest)
			if err != nil {
				panic(err)
			}

			j_doc, err := json.Marshal(job_doc)
			if err != nil {
				panic(err)
			}

			fmt.Printf("Request body: %s\nJob document: %s\n", j_req, j_doc)
		}
		panic(err)
	}

	response_body := fromJSONToMap(resp.Body)
	if printRequestBody {
		mapPrint := api.Server().PrintRequest(resp, api.ApiPath(), meta, bodyRequest, response_body)
		j, err := json.Marshal(mapPrint)
		if err != nil {
			panic(err)
		}
		fmt.Println(string(j))
	}

	fmt.Printf("[TRACE] [RESPONSE] Group id %d: %s\n", group_id, api.ApiPath())
	return response_body
}

func fromJSONToMap(respBody io.ReadCloser) map[string]interface{} {
	body, _ := ioutil.ReadAll(respBody)
	var rawMap map[string]interface{}
	err := json.Unmarshal(body, &rawMap)

	if err != nil {
		panic(err)
	}

	return rawMap
}

func mapToJsonString(values map[string]interface{}) (ret []byte) {
	ret, err := json.Marshal(values)
	if err != nil {
		panic(err)
	}
	return
}
