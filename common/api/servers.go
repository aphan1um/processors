package api

import (
	"go-processors/common"
	"net/http"
)

var JOBS_API_URL = common.GetEnv("JOBS_API_URL", "http://localhost:8000")
var GEO_API_URL = common.GetEnv("GEO_API_URL", "http://localhost:3000")

var ServerJobs = ApiServer{
	Host: JOBS_API_URL,
	PrintRequest: func(resp *http.Response, path string, meta map[string]interface{}, request_body map[string]interface{}, response_body map[string]interface{}) map[string]interface{} {
		return map[string]interface{}{
			"_apiPath":      path,
			"_responseTime": resp.Header.Get("Response-Time"),
			"reqBody":       request_body,
			"resBody":       response_body,
			"uuid":          resp.Header.Get("X-UUID"),
			"x-meta":        meta,
		}
	},
}

var ServerGeo = ApiServer{
	Host: GEO_API_URL,
	PrintRequest: func(resp *http.Response, path string, meta map[string]interface{}, request_body map[string]interface{}, response_body map[string]interface{}) map[string]interface{} {
		return map[string]interface{}{
			"_apiPath": path,
			"reqBody":  request_body,
			"resBody":  response_body,
			"x-meta":   meta,
		}
	},
}
