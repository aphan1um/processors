package postgres

import (
	"context"
	"fmt"
	"strconv"

	"github.com/jackc/pgconn"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"

	common "go-processors/common"
)

var POSTGRES_HOST = common.GetEnv("PG_HOST", "localhost")
var POSTGRES_PORT, _ = strconv.Atoi(common.GetEnv("PG_PORT", "5000"))
var POSTGRES_DB_NAME = common.GetEnv("PG_DBNAME", "postgres")
var POSTGRES_USERNAME = common.GetEnv("PG_USERNAME", "postgres")
var POSTGRES_PASSWORD = common.GetEnv("PG_PASSWORD", "dummyPass")

var Client *pgxpool.Pool

func ConnectToPostgres() {
	fmt.Printf("[Postgres] Attempting to connect to %s:%d\n", POSTGRES_HOST, POSTGRES_PORT)

	var err error // https://stackoverflow.com/a/54915997
	connStr := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", POSTGRES_HOST, POSTGRES_PORT, POSTGRES_USERNAME, POSTGRES_PASSWORD, POSTGRES_DB_NAME)
	// https://github.com/jackc/pgx/wiki/Getting-started-with-pgx-through-database-sql
	Client, err = pgxpool.Connect(context.Background(), connStr)
	if err != nil {
		panic(err)
	}

	err = Client.Ping(context.Background())
	if err != nil {
		panic(err)
	}
	fmt.Printf("[Postgres] Successfully connnected to %s:%d\n", POSTGRES_HOST, POSTGRES_PORT)

	// TODO: bad place to call it here?
	JobSiteMap = make(map[int]string)
	retrieveJobSiteIDs(&JobSiteMap)
}

func ExecuteSQL(statement string, printStatement bool, args []interface{}) pgconn.CommandTag {
	if printStatement {
		defer common.Elapsed(fmt.Sprintf("> SQL: %s\n> Params: %#v", statement, args))()
	}
	result, err := Client.Exec(context.Background(), statement, args...)

	if err != nil {
		panic(err)
	}

	return result
}

func ExecuteSQLTx(statement string, printStatement bool, args []interface{}, tx pgx.Tx) pgconn.CommandTag {
	if printStatement {
		defer common.Elapsed(fmt.Sprintf("> SQL: %s\n> Params: %#v", statement, args))()
	}
	result, err := tx.Exec(context.Background(), statement, args...)

	if err != nil {
		panic(err)
	}

	return result
}

func QuerySQLRow(statement string, printStatement bool, args []interface{}) pgx.Row {
	if printStatement {
		defer common.Elapsed(fmt.Sprintf("> SQL: %s\n> Params: %#v", statement, args))()
	}
	result := Client.QueryRow(context.Background(), statement, args...)

	return result
}

func QuerySQLRowTx(statement string, printStatement bool, args []interface{}, tx pgx.Tx) pgx.Row {
	if printStatement {
		defer common.Elapsed(fmt.Sprintf("> SQL: %s\n> Params: %#v", statement, args))()
	}
	result := tx.QueryRow(context.Background(), statement, args...)

	return result
}

func QuerySQL(statement string, printStatement bool, args []interface{}) pgx.Rows {
	if printStatement {
		defer common.Elapsed(fmt.Sprintf("> SQL: %s\n> Params: %#v", statement, args))()
	}
	result, err := Client.Query(context.Background(), statement, args...)

	if err != nil {
		panic(err)
	}

	return result
}

func QuerySQLTx(statement string, printStatement bool, args []interface{}, tx pgx.Tx) pgx.Rows {
	if printStatement {
		defer common.Elapsed(fmt.Sprintf("> SQL: %s\n> Params: %#v", statement, args))()
	}
	result, err := tx.Query(context.Background(), statement, args...)

	if err != nil {
		panic(err)
	}

	return result
}
