package postgres

import (
	"errors"
	"fmt"
	"time"

	"github.com/jackc/pgx/v4"
)

type JobDoc struct {
	Title       string
	Description string
	JobType     string
	PayType     string
	DateListed  int
	Location    interface{}
	Company     string
	Category    string
	Subcategory string
	Version     int
	JobMeta     Job
	MiscMeta    interface{}
}

type Job struct {
	JobSiteID int
	JobID     string
}

var JobSiteMap map[int]string

func ToJobDoc(docMap map[string]interface{}, jobMeta Job) (*JobDoc, error) {
	return toJobDoc[JobSiteMap[jobMeta.JobSiteID]](docMap, jobMeta)
}

var toJobDoc = map[string]func(map[string]interface{}, Job) (*JobDoc, error){
	"v2_seeks": func(m map[string]interface{}, meta Job) (*JobDoc, error) {
		parsedTime, err := getListedDate(m["date"].(map[string]interface{})["listingDate"])

		if err != nil {
			return nil, err
		}

		return &JobDoc{
			Title:       m["title"].(string),
			Description: fmt.Sprintf("%s\n%s", m["description"].(map[string]interface{})["dotpoints"].(string), m["description"].(map[string]interface{})["full"].(string)),
			JobType:     m["work_type"].(string),
			PayType:     fmt.Sprintf("%v %v", m["salary"].(map[string]interface{})["rate"], m["salary"].(map[string]interface{})["type"]),
			DateListed:  int(parsedTime.Unix()),
			Location:    m["location"].(map[string]interface{}),
			Company:     m["company"].(string),
			Category:    m["category"].(string),
			Subcategory: m["subcategory"].(string),
			JobMeta:     meta,
		}, nil
	},
	"v1_joras": func(m map[string]interface{}, meta Job) (*JobDoc, error) {
		parsedTime, err := getListedDate(m["age"])

		if err != nil {
			return nil, err
		}

		return &JobDoc{
			Title:       m["title"].(string),
			Description: m["description"].(string),
			JobType:     m["jobType"].(string),
			PayType:     m["jobType"].(string),
			DateListed:  int(parsedTime.Unix()),
			Location:    m["location"].(string),
			Company:     m["company"].(string),
			JobMeta:     meta,
		}, nil
	},
	"v1_careerones": func(m map[string]interface{}, meta Job) (*JobDoc, error) {
		payTypeStr := ""
		jobTypeStr := ""
		categoryStr := ""
		subcategoryStr := ""

		details, okDetails := m["details"]
		if okDetails {
			if details.(map[string]interface{})["workType"] != nil {
				jobTypeStr = details.(map[string]interface{})["workType"].(string)
			}

			if payDetails, okDetailsPay := details.(map[string]interface{})["pay"]; okDetailsPay && payDetails.(map[string]interface{})["type"] == "base" {
				payTypeStr = payDetails.(map[string]interface{})["amount"].(string)
			}

			categoryStr, _ = details.(map[string]interface{})["category"].(string)
			subcategoryStr, _ = details.(map[string]interface{})["occupation"].(string)
		}

		parsedTime, err := getListedDate(m["date"])

		if err != nil {
			return nil, err
		}

		return &JobDoc{
			Title:       m["title"].(string),
			Description: m["description"].(string),
			JobType:     jobTypeStr,
			PayType:     payTypeStr,
			DateListed:  int(parsedTime.Unix()),
			Location:    m["location"].(string),
			Company:     m["company"].(string),
			Category:    categoryStr,
			Subcategory: subcategoryStr,
			JobMeta:     meta,
		}, nil
	},
}

func getListedDate(dateObj interface{}) (time.Time, error) {
	var parsedTime time.Time
	var err error

	// has format 2021-04-24T09:40:40Z
	// https://stackoverflow.com/a/44233017
	switch dateListed := dateObj.(type) {
	case string:
		parsedTime, err = time.Parse(time.RFC3339, dateListed)
	case map[string]interface{}:
		parsedTime, err = time.Parse(time.RFC3339, dateListed["$date"].(string))
	case nil:
		err = errors.New("The provided time is nil")
	default:
		err = errors.New(fmt.Sprintf("Received unexpected for listing date value: %v %[1]T", dateListed))
	}

	return parsedTime, err
}

func retrieveJobSiteIDs(siteMap *map[int]string) {
	rows := QuerySQL("SELECT jobsite_id,title FROM infojobsite", true, []interface{}{})
	defer rows.Close()
	for rows.Next() {
		var (
			jobsite_id int
			title      string
		)
		rows.Scan(&jobsite_id, &title)
		(*siteMap)[jobsite_id] = title
	}
}

// Retrieve document of a job advertisement
func GetJobDocument(docMeta Job, verbose bool, tx pgx.Tx) (JobDoc, bool) {
	// https://www.alexedwards.net/blog/using-postgresql-jsonb
	var jsonData = make(map[string]interface{})

	var err error
	if tx != nil {
		err = QuerySQLRowTx("SELECT data FROM jobs WHERE job_web_id = $1 AND jobsite_id = $2", verbose, []interface{}{docMeta.JobID, docMeta.JobSiteID}, tx).Scan(&jsonData)
	} else {
		err = QuerySQLRow("SELECT data FROM jobs WHERE job_web_id = $1 AND jobsite_id = $2", verbose, []interface{}{docMeta.JobID, docMeta.JobSiteID}).Scan(&jsonData)
	}
	if err != nil {
		panic(err)
	}

	jobDoc, err := ToJobDoc(jsonData, docMeta)
	return *jobDoc, true
}
