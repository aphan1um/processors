package main

import (
	"math"

	"github.com/kelindar/dbscan"
)

type UnixTime int

func (u UnixTime) DistanceTo(other dbscan.Point) float64 {
	return math.Abs(float64(u) - float64(other.(UnixTime)))
}

func (u UnixTime) Name() string {
	return string(u)
}
