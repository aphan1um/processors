package main

import (
	"context"
	pg "go-processors/common/postgres"
)

var postgresJobTypeMap = make(map[string]int)
var postgresEducationLevelMap = make(map[string]int)
var postgresAusStatesList = make([]string, 0)
var postgresFlagMap = make(map[int]string)

func retrieveDataTables() {
	_retrieveJobTypeValues()
	_retrieveEducationLevelValues()
	_retrieveAusStatesValues()
	_retrieveFlags()
}

func _retrieveJobTypeValues() {
	rows, _ := pg.Client.Query(context.Background(), "SELECT jobtype_id,jobtype FROM infojobtype")
	defer rows.Close()
	for rows.Next() {
		var (
			jobtype_id int
			jobtype    string
		)
		rows.Scan(&jobtype_id, &jobtype)
		postgresJobTypeMap[jobtype] = jobtype_id
	}
}

func _retrieveEducationLevelValues() {
	rows, _ := pg.Client.Query(context.Background(), "SELECT edulevel_id,title FROM infoeducationlevel")
	defer rows.Close()
	for rows.Next() {
		var (
			edulevel_id int
			title       string
		)
		rows.Scan(&edulevel_id, &title)
		postgresEducationLevelMap[title] = edulevel_id
	}
}

func _retrieveAusStatesValues() {
	// order by important to allow binary search
	rows, _ := pg.Client.Query(context.Background(), "SELECT state_code FROM infostates ORDER BY state_code")
	defer rows.Close()
	for rows.Next() {
		var (
			state_code string
		)
		rows.Scan(&state_code)
		postgresAusStatesList = append(postgresAusStatesList, state_code)
	}
}

func _retrieveFlags() {
	rows, _ := pg.Client.Query(context.Background(), "SELECT flag_id,flag_name FROM datumflags")
	defer rows.Close()
	for rows.Next() {
		var (
			flag_id   int
			flag_name string
		)
		rows.Scan(&flag_id, &flag_name)
		postgresFlagMap[flag_id] = flag_name
	}
}
