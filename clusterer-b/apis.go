package main

// var apiOperationsMap = map[string]api.ApiOperation{
// 	"FLAG_DATE_LISTED": apiDateListed,
// 	"FLAG_LOCATION":    apiLocation,
// }

// var apiDateListed = api.ApiOperation{
// 	ApiPath: "/date",
// 	Server:  serverJobs,
// 	GetBodyRequest: func(job_doc pgUtils.JobDoc, collectionName string, group_id int) map[string]interface{} {
// 		return map[string]interface{}{
// 			"number": job_doc.DateListed,
// 		}
// 	},
// 	PreSelectDoc:  nil,
// 	GotEnoughInfo: func(m map[string]interface{}) bool { return true },
// 	UpdateDatabase: func(response map[string]interface{}, group_id int, tx pgx.Tx) {
// 		_, err := tx.Exec(
// 			context.Background(),
// 			"INSERT INTO jobdatum (group_id, config_id, date_listed) VALUES ($1, $2, $3) ON CONFLICT (group_id, config_id) DO UPDATE SET date_listed = $3",
// 			group_id, PROCESSOR_CONFIG_PROFILE,
// 			response["date_listed"].(string))
// 		if err != nil {
// 			panic(err)
// 		}
// 	},
// }

// var apiLocation = api.ApiOperation{
// 	ApiPath: "/location",
// 	Server:  serverGeo,
// 	GetBodyRequest: func(job_doc pgUtils.JobDoc, collectionName string, group_id int) map[string]interface{} {
// 		return map[string]interface{}{
// 			"location":    job_doc.Location,
// 			"description": job_doc.Description,
// 		}
// 	},
// 	PreSelectDoc:  nil,
// 	GotEnoughInfo: func(m map[string]interface{}) bool { return m["info"].(map[string]interface{})["state"] != nil },
// 	UpdateDatabase: func(response map[string]interface{}, group_id int, tx pgx.Tx) {
// 		var stateValue *string
// 		if response["info"].(map[string]interface{})["isEmpty"].(bool) {
// 			stateValue = nil
// 		} else {
// 			stateValue = new(string)
// 			*stateValue = response["info"].(map[string]interface{})["state"].(string)
// 		}

// 		_, err := tx.Exec(
// 			context.Background(),
// 			"INSERT INTO jobdatum (group_id, config_id, state_code) VALUES ($1, $2, $3) ON CONFLICT (group_id, config_id) DO UPDATE SET state_code = $3",
// 			group_id, PROCESSOR_CONFIG_PROFILE, stateValue,
// 		)
// 		if err != nil {
// 			panic(err)
// 		}
// 	},
// }
