package main

import (
	"encoding/json"
)

func mapToJsonString(values map[string]interface{}) (ret []byte) {
	ret, err := json.Marshal(values)
	if err != nil {
		panic(err)
	}
	return
}
