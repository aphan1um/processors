package main

import (
	"context"
	"fmt"
	"regexp"
	"sort"

	pg "go-processors/common/postgres"

	"github.com/jackc/pgx/v4"
)

var reMultipleSpaces = regexp.MustCompile(`  +`)

func sqlInsertPhrases(phrases map[string]interface{}, group_id int, phraseType string, tx pgx.Tx) {
	for phrase, value := range phrases {
		var phrase_id int
		phrasetype_id := postgresPhraseTypeMap[phraseType]

		// see if phrase exists
		err := pg.QuerySQLRowTx(
			"SELECT phrase_id FROM phrases WHERE phrase_name = $1 AND phrasetype_id = $2",
			false, []interface{}{phrase, phrasetype_id}, tx).Scan(&phrase_id)

		if err == pgx.ErrNoRows { // if phrase hasn't been added yet
			err = pg.QuerySQLRowTx(
				"INSERT INTO phrases (phrasetype_id, phrase_name) VALUES ($1, $2) RETURNING phrase_id",
				false,
				[]interface{}{phrasetype_id, phrase},
				tx).Scan(&phrase_id)
		}
		if err != nil {
			panic(err)
		}

		fmt.Printf("[Phrase] Phrase '%s' (type: %s, type ID: %d) found with phrase ID %d and value %f\n", phrase, phraseType, phrasetype_id, phrase_id, value)

		// now we can add phrase
		_, err = tx.Exec(
			context.Background(),
			`INSERT INTO jobphrases (group_id, phrase_id, value) VALUES ($1, $2, $3)
			ON CONFLICT (group_id, phrase_id) DO UPDATE SET value = $3`,
			group_id, phrase_id, value)
		if err != nil {
			panic(err)
		}
	}
}

func getGroupIdFromJobId(job_id string) int {
	var ret int
	err := pg.Client.QueryRow(context.Background(), fmt.Sprintf("SELECT group_id AS ret FROM jobs WHERE job_id = '%s'", job_id)).Scan(&ret)
	if err != nil {
		panic(err)
	}
	return ret
}

func retrieveJobMetaFromGroupID(group_id int, tx pgx.Tx) []pg.Job {
	ret := make([]pg.Job, 0)

	var rows pgx.Rows

	if tx == nil {
		rows = pg.QuerySQL(
			`SELECT job_web_id,jobsite_id
			FROM jobs
			WHERE job_id IN (
				SELECT MIN(j.job_id) FROM jobs j
				INNER JOIN "ClusterJobList" ON (j.job_id = "ClusterJobList".job_id)
				WHERE "ClusterJobList".group_id = $1
				GROUP BY j.jobsite_id
			)
			ORDER BY jobsite_id ASC`,
			false, []interface{}{group_id})
	} else {
		rows = pg.QuerySQLTx(
			`SELECT job_web_id,jobsite_id
			FROM jobs
			WHERE job_id IN (
				SELECT MIN(j.job_id) FROM jobs j
				INNER JOIN "ClusterJobList" ON (j.job_id = "ClusterJobList".job_id)
				WHERE "ClusterJobList".group_id = $1
				GROUP BY j.jobsite_id
			)
			ORDER BY jobsite_id ASC`,
			false, []interface{}{group_id}, tx)
	}

	defer rows.Close()

	for rows.Next() {
		var job_web_id string
		var jobsite_id int

		if err := rows.Scan(&job_web_id, &jobsite_id); err != nil {
			panic(err)
		}

		ret = append(ret, pg.Job{JobSiteID: jobsite_id, JobID: job_web_id})
	}

	return ret
}

func jobGroupInsertGeo(response map[string]interface{}, group_id int, tx pgx.Tx) {
	var aus_state_str string

	if response["info"].(map[string]interface{})["state"] == nil {
		aus_state_str = "error"
		// TODO: DEBUG
		panic("Unable to parse Australian state via Geo API")
	} else {
		aus_state_str = response["info"].(map[string]interface{})["state"].(string)

		if sort.SearchStrings(postgresAusStatesList, aus_state_str) < 0 {
			panic(fmt.Sprintf("Unable to find mapping for Australian state: %s", aus_state_str))
		}
	}

	pg.ExecuteSQLTx(
		"INSERT INTO jobdatum (group_id, state_code) VALUES ($1, $2)",
		true,
		[]interface{}{
			group_id,
			aus_state_str,
		}, tx)
}

func removeTaskFromQueue(group_id int, flag_id int, tx pgx.Tx) {
	_, err := tx.Exec(context.Background(), "DELETE FROM datumqueue WHERE group_id = $1 AND flag_id = $2", group_id, flag_id)
	if err != nil {
		panic(err)
	}
}
