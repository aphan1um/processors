package main

import (
	"go-processors/common"
	"strconv"
)

const MAX_ITEMS_TO_PROCESS = 5

// https://shiroyasha.io/selecting-for-share-and-update-in-postgresql.html
var DATUM_QUEUE_ITEM_QUERY = `SELECT group_id, flag_id
	FROM datumqueue
	LIMIT 1
	FOR UPDATE OF datumqueue SKIP LOCKED`

var DISCRIMINATING_PHRASE_TYPES = []string{"gender", "disability"}

var JOBS_API_URL = common.GetEnv("JOBS_API_URL", "http://localhost:8000")
var GEO_API_URL = common.GetEnv("GEO_API_URL", "http://localhost:3000")

var DELAY_LOOP_MS, _ = strconv.Atoi(common.GetEnv("DELAY_LOOP_MS", "500"))
var DELAY_LOOP_NO_ITEMS_MS, _ = strconv.Atoi(common.GetEnv("DELAY_LOOP_NO_ITEMS_MS", "10000"))

var PROCESSOR_CONFIG_PROFILE = common.GetEnv("PROCESSOR_CONFIG_PROFILE", "")

var DEBUG_MODE = len(common.GetEnv("DEBUG_MODE", "")) > 0
