package main

import (
	"context"
	"errors"
	"fmt"
	"time"

	pg "go-processors/common/postgres"

	"github.com/jackc/pgx/v4"
)

var guard = make(chan struct{}, MAX_ITEMS_TO_PROCESS)

var db_context = context.Background()

func main() {
	pg.ConnectToPostgres()

	loadProfile()
	fmt.Println("[DEBUG] Profile loaded")
	fmt.Printf("[DEBUG] %v\n", SOFTSKILL_CONFIG)

	retrieveDataTables()
	fmt.Printf("[DEBUG] Retrieved all data tables\n")

	go func() {
		for {
			time.Sleep(time.Duration(60000) * time.Millisecond)
			fmt.Printf("[DEBUG] Tasks channel: %d\n", len(guard))
		}
	}()

	for { // loop
		// get group id to work with, as well as the corresponding SQL transaction
		group_id, flag_id, txTask := beginTransaction()
		fmt.Printf("[INFO] Got a group id to work with: %v (task - %s)\n", group_id, postgresFlagMap[flag_id])

		// get all job adverts in this job group
		jobsList := retrieveJobMetaFromGroupID(group_id, nil)
		fmt.Printf("[INFO] Retrieved jobs list for group id %d: %v\n", group_id, jobsList)

		guard <- struct{}{}

		go func(flag_id int) {
			processJobList(jobsList, group_id, flag_id, txTask)

			// delete task from queue
			removeTaskFromQueue(group_id, flag_id, txTask)

			// commit transaction for job group
			if err := txTask.Commit(context.Background()); err != nil {
				panic(err)
			}

			<-guard
			fmt.Printf("[INFO] Successfully parsed job group: %d\n", group_id)
		}(flag_id)

		time.Sleep(time.Duration(DELAY_LOOP_MS) * time.Millisecond)
	}
}

func processJobList(jobsList []pg.Job, group_id int, flag_id int, tx pgx.Tx) {
	var prevResponse map[string]interface{}
	var preselectedJob *pg.Job = nil
	// var apiGeoResponse map[string]interface{} // TODO

	api := apiOperationsMap[postgresFlagMap[flag_id]]
	fmt.Printf("[TRACE] [START] Group id %d: %s, Flag id: %d (%s)\n", group_id, api.ApiPath, flag_id, postgresFlagMap[flag_id])

	if api.PreSelectDoc != nil {
		for _, job := range jobsList {
			job_doc, _ := pg.GetJobDocument(job, false, nil)
			if api.PreSelectDoc(job_doc) {
				preselectedJob = &job
				break
			}
		}
	}

	if preselectedJob != nil {
		job_doc, _ := pg.GetJobDocument(*preselectedJob, false, nil)

		response := callAPI(
			api, job_doc, group_id,
			map[string]interface{}{"group_id": group_id, "web_id": job_doc.JobMeta.JobID, "preselected": true},
			true)

		api.UpdateDatabase(response, group_id, tx)
	} else {
		for i, job := range jobsList {
			// fmt.Printf("> Getting job document '%s' from site: '%s'\n", job.JobID, pg.JobSiteMap[job.JobSiteID])
			job_doc, _ := pg.GetJobDocument(job, false, nil)

			response := callAPI(
				api, job_doc, group_id,
				map[string]interface{}{"group_id": group_id, "web_id": job_doc.JobMeta.JobID, "iteration": i},
				true)

			if api.GotEnoughInfo(response) {
				api.UpdateDatabase(response, group_id, tx)
				break
			} else {
				prevResponse = response
			}

			if len(jobsList) == i+1 {
				api.UpdateDatabase(prevResponse, group_id, tx)
				break
			} else {
				fmt.Printf("[TRACE] [processJobList] Group id %d: %s Didn't get enough info\n", group_id, api.ApiPath)
			}
		}
	}

	fmt.Printf("[TRACE] [processJobList] Group id %d: %s finalising flag value\n", group_id, api.ApiPath)
	// update flag for job group that the feature has been processed
	_, err := tx.Exec(
		context.Background(),
		"UPDATE jobdatum SET flag = flag | $3 WHERE group_id = $1 AND config_id = $2",
		group_id, PROCESSOR_CONFIG_PROFILE, 1<<flag_id)
	if err != nil {
		panic(err)
	}

	fmt.Printf("[TRACE] [END] Group id %d: %s\n", group_id, api.ApiPath)
}

func beginTransaction() (int, int, pgx.Tx) {
	var group_id int
	var flag_id int

	tx, err := pg.Client.BeginTx(db_context, pgx.TxOptions{})
	if err != nil {
		panic(err)
	}

	err = pg.QuerySQLRowTx(DATUM_QUEUE_ITEM_QUERY, false, []interface{}{}, tx).Scan(&group_id, &flag_id)

	for err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			fmt.Printf("[WARN] No new task from jobdatum found, waiting %d ms instead...\n", DELAY_LOOP_NO_ITEMS_MS)
			time.Sleep(time.Duration(DELAY_LOOP_NO_ITEMS_MS) * time.Millisecond)
		} else {
			panic(err)
		}

		err = tx.Commit(context.Background())
		if err != nil {
			panic(err)
		}

		tx, err = pg.Client.BeginTx(db_context, pgx.TxOptions{})
		if err != nil {
			panic(err)
		}

		err = pg.QuerySQLRowTx(DATUM_QUEUE_ITEM_QUERY, false, []interface{}{}, tx).Scan(&group_id, &flag_id)
	}

	return group_id, flag_id, tx
}

func loadProfile() {
	var profile map[string]interface{}

	if len(PROCESSOR_CONFIG_PROFILE) == 0 {
		panic("PROCESSOR_CONFIG_PROFILE is empty or not provided")
	}

	err := pg.Client.QueryRow(
		context.Background(),
		"SELECT meta FROM \"ClusterConfig\" WHERE config_id = $1",
		PROCESSOR_CONFIG_PROFILE).Scan(&profile)
	if err != nil {
		panic(err)
	}

	SOFTSKILL_CONFIG = profile["softskills"].(map[string]interface{})
}
